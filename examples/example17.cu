/*! \file
 *  \brief Example of two graph creation and embed one into the other.
 */

#include <iostream>

#include <chrono>
#include <random>
#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/algorithm/r_kleene.cuh>
#include <vnegpu/algorithm/k_means.cuh>
#include <vnegpu/distance.cuh>
#include <vnegpu/generator/request.cuh> //Fat tree generator
#include <vnegpu/generator/dcell.cuh> //Fat tree generator
#include <vnegpu/generator/bcube.cuh> //Fat tree generator
#include <vnegpu/generator/dcell.cuh> //Fat tree generator
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator


int main(){

  try //There are some runtime exceptions
  {


    //int n=3;
    //int v[3] = {5, 4, 9};
    //vnegpu::graph<float, vnegpu::graph_type::real_tests>* request = vnegpu::generator::request_gen<float, vnegpu::graph_type::real_tests>(n, v);
    vnegpu::graph<float, vnegpu::graph_type::real_tests>* data_center = vnegpu::generator::dcell<float, vnegpu::graph_type::real_tests>(11, 2);
    data_center = vnegpu::generator::bcube<float, vnegpu::graph_type::real_tests>(7, 4);
    data_center = vnegpu::generator::fat_tree<float, vnegpu::graph_type::real_tests>(40);
    //data_center->save_to_gexf("example17.gexf");
    /*
    //Set all the edge_capacity variables to one.
    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_band, i, 1000);
    }

    //Update the graph on GPU. (the generator is only done on Host memory)
    data_center->update_gpu();

    vnegpu::algorithm::r_kleene(data_center, data_center->variables.edge_band);

    vnegpu::algorithm::k_means(data_center, 70, vnegpu::distance::matrix_based());

    //Update the graph on CPU (the algorithm is only done on Device memory)
    data_center->update_cpu();

    //Save the graph on the GEXF format with file name example6.gexf
    data_center->save_to_gexf("example17.gexf");

//    printf("n: %d e: %d\n", request->get_num_nodes(), request->get_num_edges());
//    for(int z=0; z<request->get_num_edges(); z++){
//      request->set_variable_edge_undirected(request->variables.edge_band, z, 1.0f);
//    }

/*    request->set_variable_node(0, 0, 13);
    request->set_variable_node(0, 1, 14);
    request->set_variable_edge_undirected(request->variables.edge_band, 0, 2.0f);

    vnegpu::generator::shuffle(request);
*/
/*
    request->update_gpu();

    vnegpu::algorithm::mcl(request, request->variables.edge_band, 2, 1.3, 0.000001);

    request->update_cpu();

    request->save_to_gexf("a_example17.gexf");
*/

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

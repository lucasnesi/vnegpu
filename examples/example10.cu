/*! \file
 *  \brief Example of graph creation and apply a grouping/clustering algorithm.
 */

#include <iostream>

#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/distance.cuh> //Pre-defined metrics
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator
#include <vnegpu/algorithm/serial/r_kleene.cuh> //R-Kleene algorithm
#include <vnegpu/algorithm/serial/k_means.cuh> //k_means algorithm

int main(){

  try //There are some runtime exceptions
  {
    //Generating the data center graph based on Fat tree topology k=18
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(6);

    //Set all the edge_capacity variables to one.
    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 1);
    }

    //Update the graph on GPU. (the generator is only done on Host memory)
    data_center->update_gpu();

    vnegpu::util::host_matrix<float>* distance_matrix = vnegpu::algorithm::serial::r_kleene(data_center, data_center->variables.edge_capacity);

    vnegpu::algorithm::serial::k_means(data_center, 6, vnegpu::distance::serial::matrix_based(), distance_matrix);

    //Update the graph on CPU (the algorithm is only done on Device memory)


    //Save the graph on the GEXF format with file name example6.gexf
    data_center->save_to_gexf("example10.gexf");

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

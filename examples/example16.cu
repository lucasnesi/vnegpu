/*! \file
 *  \brief Example of two graph creation and embed one into the other.
 */

#include <iostream>

#include <chrono>
#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/generator/request.cuh> //Fat tree generator
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator
#include <vnegpu/algorithm/fit.cuh> //Fit algorithm
#include <vnegpu/allocation_policies.cuh> //Allocation polocies used in the fit algorithm


int main(){

  try //There are some runtime exceptions
  {
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(6);

    int n=2;
    int v[2] = {2, 3};
    vnegpu::graph<float>* request = vnegpu::generator::request_gen<float>(n, v);



    //Set the node_capacity variable of data_center graph.
    for(int i=0; i<data_center->get_num_nodes(); i++){
      if(data_center->get_node_type(i) == vnegpu::TYPE_HOST){
        //If the node is a host set to 10
        data_center->set_variable_node(data_center->variables.node_capacity, i, 10);
      }else{
        //else set to 1
        data_center->set_variable_node(data_center->variables.node_capacity, i, 1);
      }
    }

    //Set all edge_capacity variables of data_center graph to one.
    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 10);
    }

    //Set the node_capacity variable of request graph.
    for(int i=0; i<request->get_num_nodes(); i++){
      if(request->get_node_type(i) == vnegpu::TYPE_HOST)
      {
        //If the node is a host set to 1
        request->set_variable_node(request->variables.node_capacity, i, 1);
      }else{
        //else set to 0.1
        request->set_variable_node(request->variables.node_capacity, i, 0.1);
      }
    }

    //Set all edge_capacity variables of request graph to one.
    for(int i=0; i<request->get_num_edges(); i++){
      request->set_variable_edge_undirected(request->variables.edge_capacity, i, 1);
    }

    data_center->save_to_gexf("example16_data_0.gexf");
    request->save_to_gexf("example16_request_0.gexf");

    data_center->update_gpu();
    request->update_gpu();

    vnegpu::algorithm::fit(data_center, request, vnegpu::alocation::worst_fit());

    data_center->update_cpu();
    request->update_cpu();

    data_center->save_to_gexf("example16_data_1.gexf");
    request->save_to_gexf("example16_request_1.gexf");

    vnegpu::algorithm::detail::desalloc_imp(data_center, request, vnegpu::alocation::worst_fit());

    data_center->save_to_gexf("example16_data_2.gexf");
    request->save_to_gexf("example16_request_2.gexf");

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

/*! \file
 *  \brief Example of graph creation and apply an ASPS algorithm.
 */

#include <iostream>

#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator
#include <vnegpu/algorithm/r_kleene.cuh> //R-Kleene algorithm

int main(){

  try //There are some runtime exceptions
  {
    //Generating the data center graph based on Fat tree topology k=24
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(24);

    //Set all edge_capacity variables to one.
    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 1);
    }

    //Update the graph on GPU. (the generator is only done on Host memory)
    data_center->update_gpu();

    //Running the r-kleene algorithm on GPU, on the data_center graph,
    //using the edge capacity variable as base of distance/legth/weight between edges,
    //the result is saved on the graph structure element distance_matrix.
    vnegpu::algorithm::r_kleene(data_center, data_center->variables.edge_capacity);

    //Update the graph on CPU (the algorithm is only done on Device memory)
    data_center->update_cpu();

    //Save the graph on the GEXF format with file name example5.gexf
    data_center->save_to_gexf("example5.gexf");

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

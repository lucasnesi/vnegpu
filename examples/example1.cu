/*! \file
 *  \brief Example of graph creation and apply a local metric.
 */

#include <iostream>

#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/metrics.cuh> //Pre-defined metrics
#include <vnegpu/generator/bcube.cuh> //Bcube generator
#include <vnegpu/algorithm/local_metric.cuh> //Local metric algorithm

int main(){

  try //There are some runtime exceptions
  {
    //Generating the data center graph based on Bcube topology N=5 K=3
    vnegpu::graph<float>* data_center = vnegpu::generator::bcube<float>(5,3);

    //Adding a new node variable nammed Degree
    int metric_index = data_center->add_node_variable("Degree");

    //Update the graph on GPU. (the generator is only done on Host memory)
    data_center->update_gpu();

    //Running the local metric algorithm on GPU, on the data_center graph, saving
    //the results on the mentric_index and using the available functor degree
    vnegpu::algorithm::local_metric(data_center, metric_index, vnegpu::metrics::degree() );

    //Update the graph on CPU (the algorithm is only done on Device memory)
    data_center->update_cpu();

    //Save the graph on the GEXF format with file name example1.gexf
    data_center->save_to_gexf("example1.gexf");

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

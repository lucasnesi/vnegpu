/*! \file
 *  \brief Example of graph creation and apply a generic rank.
 */

#include <iostream>

#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/metrics.cuh> //Pre-defined metrics
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator
#include <vnegpu/algorithm/generic_rank.cuh> //Generic rank algorithm

int main(){

  try //There are some runtime exceptions
  {
    //Generating the data center graph based on Fat tree topology k=6
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(6);

    //Adding a new node variable nammed Rank
    int rank_index = data_center->add_node_variable("Rank");

    //Update the graph on GPU. (the generator is only done on Host memory)
    data_center->update_gpu();

    //Running the generic rank algorithm on GPU, on the data_center graph, saving
    //the results on the rank_index and using the available functor one
    vnegpu::algorithm::generic_rank(data_center, rank_index, vnegpu::metrics::one() );

    //Update the graph on CPU (the algorithm is only done on Device memory)
    data_center->update_cpu();

    //Save the graph on the GEXF format with file name example2.gexf
    data_center->save_to_gexf("example2.gexf");

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

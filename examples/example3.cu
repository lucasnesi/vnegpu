/*! \file
 *  \brief Example of graph creation and apply a SSSP algorithm.
 */

#include <iostream>

#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/metrics.cuh> //Pre-defined metrics
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator
#include <vnegpu/algorithm/dijkstra.cuh> //Dijkstra algorithm

int main(){

  try //There are some runtime exceptions
  {
    //Generating the data center graph based on Fat tree topology k=6
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(6);

    //Set the initial node id used in the SSSP.
    int initial_node = 0;

    //Adding a new node variable nammed Distance from 0
    int distance_index = data_center->add_node_variable("Distance from 0");

    //Set all edge_capacity variables to one.
    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 1);
    }

    //Update the graph on GPU. (the generator is only done on Host memory)
    data_center->update_gpu();

    //Running the dijkstra algorithm on GPU, on the data_center graph,
    //using the initial_node as the initial node on the SSSP algorithm, saving
    //the results on the distance_index and using the available functor edge_weight
    //that consider edge_capacity as a measure of length/weight.
    vnegpu::algorithm::dijkstra(data_center, initial_node, distance_index, vnegpu::metrics::edge_weight());

    //Update the graph on CPU (the algorithm is only done on Device memory)
    data_center->update_cpu();

    //Save the graph on the GEXF format with file name example3.gexf
    data_center->save_to_gexf("example3.gexf");

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

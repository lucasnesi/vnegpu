/*! \file
 *  \brief Example of two graph creation and embed one into the other.
 */

#include <iostream>

#include <chrono>
#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/algorithm/mcl.cuh> //MCL algorithm
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator
#include <vnegpu/algorithm/fit.cuh> //Fit algorithm
#include <vnegpu/allocation_policies.cuh> //Allocation polocies used in the fit algorithm
#include <vnegpu/util/group.cuh>

int main(){

  try //There are some runtime exceptions
  {
    //Generating the data center graph based on Fat tree topology k=6
    vnegpu::graph<float, vnegpu::graph_type::minimalist_rank>* data_center = vnegpu::generator::fat_tree<float, vnegpu::graph_type::minimalist_rank>(6);

    //Generating the request graph based on Fat tree topology k=4
    vnegpu::graph<float, vnegpu::graph_type::minimalist_rank>* request = vnegpu::generator::fat_tree<float, vnegpu::graph_type::minimalist_rank>(4);

    //Set the node_capacity variable of data_center graph.
    for(int i=0; i<data_center->get_num_nodes(); i++){
      if(data_center->get_node_type(i) == vnegpu::TYPE_HOST){
        //If the node is a host set to 10
        data_center->set_variable_node(data_center->variables.node_capacity, i, 10);
      }else{
        //else set to 1
        data_center->set_variable_node(data_center->variables.node_capacity, i, 1);
      }
    }

    //Set all edge_capacity variables of data_center graph to one.
    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 15);
    }

    //Set the node_capacity variable of request graph.
    for(int i=0; i<request->get_num_nodes(); i++){
      if(request->get_node_type(i) == vnegpu::TYPE_HOST)
      {
        //If the node is a host set to 1
        request->set_variable_node(request->variables.node_capacity, i, 1);
      }else{
        //else set to 0.1
        request->set_variable_node(request->variables.node_capacity, i, 0.1);
      }
    }

    //Set all edge_capacity variables of request graph to one.
    for(int i=0; i<request->get_num_edges(); i++){
      request->set_variable_edge_undirected(request->variables.edge_capacity, i, 1);
    }



  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

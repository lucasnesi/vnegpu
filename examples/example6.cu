/*! \file
 *  \brief Example of graph creation and apply a grouping/clustering algorithm.
 */

#include <iostream>

#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator
#include <vnegpu/algorithm/mcl.cuh> //MCL algorithm

int main(){

  try //There are some runtime exceptions
  {
    //Generating the data center graph based on Fat tree topology k=18
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(18);

    //Set all the edge_capacity variables to one.
    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 1);
    }

    //Update the graph on GPU. (the generator is only done on Host memory)
    data_center->update_gpu();

    //Running the MCL algorithm on GPU, on the data_center graph,
    //using the edge capacity variable as base of distance/legth/weight between edges,
    //with factor p of 2 (indifferent for now), r factor 1.2 (mcl author recommend r
    //factor between 1.2 and 5, this control the granularity of the groups) and with
    //max error between iterations to end the algorithm = 0.
    //the result is saved on the graph structure element group_id.
    vnegpu::algorithm::mcl(data_center, data_center->variables.edge_capacity, 2, 1.2, 0);

    //Update the graph on CPU (the algorithm is only done on Device memory)
    data_center->update_cpu();

    //Save the graph on the GEXF format with file name example6.gexf
    data_center->save_to_gexf("example6.gexf");

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

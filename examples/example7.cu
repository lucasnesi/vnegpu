/*! \file
 *  \brief Example of two graph creation and embed one into the other.
 */

#include <iostream>

#include <chrono>
#include <vnegpu/graph.cuh> //Main data structure
#include <vnegpu/generator/fat_tree.cuh> //Fat tree generator
#include <vnegpu/algorithm/fit.cuh> //Fit algorithm
#include <vnegpu/allocation_policies.cuh> //Allocation polocies used in the fit algorithm

int main(){

  try //There are some runtime exceptions
  {
    //Generating the data center graph based on Fat tree topology k=6
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(48);

    //Generating the request graph based on Fat tree topology k=4
    vnegpu::graph<float>* request = vnegpu::generator::fat_tree<float>(12);

    //Set the node_capacity variable of data_center graph.
    for(int i=0; i<data_center->get_num_nodes(); i++){
      if(data_center->get_node_type(i) == vnegpu::TYPE_HOST){
        //If the node is a host set to 10
        data_center->set_variable_node(data_center->variables.node_capacity, i, 10);
      }else{
        //else set to 1
        data_center->set_variable_node(data_center->variables.node_capacity, i, 1);
      }
    }

    //Set all edge_capacity variables of data_center graph to one.
    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 100);
    }

    //Set the node_capacity variable of request graph.
    for(int i=0; i<request->get_num_nodes(); i++){
      if(request->get_node_type(i) == vnegpu::TYPE_HOST)
      {
        //If the node is a host set to 1
        request->set_variable_node(request->variables.node_capacity, i, 1);
      }else{
        //else set to 0.1
        request->set_variable_node(request->variables.node_capacity, i, 0.1);
      }
    }

    //Set all edge_capacity variables of request graph to one.
    for(int i=0; i<request->get_num_edges(); i++){
      request->set_variable_edge_undirected(request->variables.edge_capacity, i, 1);
    }

    //Update both graphs to GPU.
    data_center->update_gpu();
    request->update_gpu();

    //Running the Fit algorithm on GPU, with the data_center graph as substrate,
    //and with the request graph as virtual network to be embed.
    //using the worst_fit police.
    //The result is the decrease of the variables on the substrate graph, and
    //the ids of the parents/host elements (of the substrate graph) in the request
    //graph elements allocation_to_nodes_ids and allocation_to_edges_ids
    auto start = std::chrono::steady_clock::now();

    vnegpu::algorithm::fit(data_center, request, vnegpu::allocation::worst_fit());

    auto end = std::chrono::steady_clock::now();

    auto diff = end - start;

    std::cout << std::chrono::duration <double, std::milli> (diff).count() << " ms" << std::endl;

    //Update both graphs to CPU.
    data_center->update_cpu();
    request->update_cpu();

    //Save both graphs
    data_center->save_to_gexf("example7_data_center.gexf");
    request->save_to_gexf("example7_request.gexf");

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

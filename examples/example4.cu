/*! \file
 *  \brief Example of using the secondary data structure matrix.
 */

#include <iostream>

#include <vnegpu/util/matrix.cuh> //Matrix data structure

int main(){

  try //There are some runtime exceptions
  {
    //Creating matrix testA 4x2 fill it with 1 and print it
    vnegpu::util::matrix<float>* testA = new vnegpu::util::matrix<float>(4,2);
    testA->host_debug_fill_one();
    testA->host_debug_print();

    //Creating matrix testB 2x4 fill it with 1 and print it
    vnegpu::util::matrix<float>* testB = new vnegpu::util::matrix<float>(2,4);
    testB->host_debug_fill_one();
    testB->host_debug_print();

    //Generating matrix testC from the multiplication of testA x testB and print it
    vnegpu::util::matrix<float>* testC = testA->mul(testB);
    testC->host_debug_print();

  } catch (const std::exception& e) {
    std::cout << "Error: " << e.what();
  }

  return 0;
}

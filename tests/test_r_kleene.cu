#define BOOST_TEST_MODULE R_Kleene

#include <boost/test/included/unit_test.hpp>

#include <vnegpu/graph.cuh>
#include <vnegpu/generator/fat_tree.cuh>
#include <vnegpu/metrics.cuh>
#include <vnegpu/util/host_matrix.cuh>
#include <vnegpu/algorithm/r_kleene.cuh>
#include <vnegpu/algorithm/serial/r_kleene.cuh>

BOOST_AUTO_TEST_CASE(R_Kleene)
{
  try
  {
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(6);

    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 1);
    }

    data_center->update_gpu();

    vnegpu::algorithm::r_kleene(data_center, data_center->variables.edge_capacity);

    data_center->update_cpu();

    vnegpu::util::host_matrix<float>* matrix_cpu = vnegpu::algorithm::serial::r_kleene(data_center, data_center->variables.edge_capacity);

    vnegpu::util::host_matrix<float>* matrix_gpu = new vnegpu::util::host_matrix<float>(data_center->get_distance_matrix());

    BOOST_TEST( matrix_gpu->comp(matrix_cpu) );

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }



}

#define BOOST_TEST_MODULE Dijkstra

#include <boost/test/included/unit_test.hpp>

#include <vnegpu/graph.cuh>
#include <vnegpu/generator/fat_tree.cuh>
#include <vnegpu/metrics.cuh>
#include <vnegpu/algorithm/dijkstra.cuh>
#include <vnegpu/algorithm/serial/dijkstra.cuh>

BOOST_AUTO_TEST_CASE(Dijkstra)
{
  try
  {
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(6);

    int distance_gpu_index = data_center->add_node_variable("GPU Distance");

    int distance_cpu_index = data_center->add_node_variable("CPU Distance");

    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 1);
    }

    int initial_node = 0;

    data_center->update_gpu();

    vnegpu::algorithm::dijkstra(data_center, initial_node, distance_gpu_index, vnegpu::metrics::edge_weight());

    data_center->update_cpu();

    vnegpu::algorithm::serial::dijkstra(data_center, initial_node, distance_cpu_index, vnegpu::metrics::edge_weight());

    for(int id=0; id<data_center->get_num_nodes(); id++)
    {
       float V_GPU = data_center->get_variable_node(distance_gpu_index, id);
       float V_CPU = data_center->get_variable_node(distance_cpu_index, id);
       BOOST_TEST( V_GPU == V_CPU );
    }

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }



}

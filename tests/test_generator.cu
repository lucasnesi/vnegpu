#define BOOST_TEST_MODULE Generator

#include <boost/test/included/unit_test.hpp>

#include <vnegpu/graph.cuh>
#include <vnegpu/generator/fat_tree.cuh>

BOOST_AUTO_TEST_CASE(fat_tree)
{
  vnegpu::graph<float>* new_fat_tree = vnegpu::generator::fat_tree<float>(6);
  BOOST_TEST(new_fat_tree);
  BOOST_TEST(new_fat_tree->get_num_nodes() == 99);
  BOOST_TEST(new_fat_tree->get_num_edges() == 162);
}

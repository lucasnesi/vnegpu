#define BOOST_TEST_MODULE Local_metric

#include <boost/test/included/unit_test.hpp>

#include <vnegpu/graph.cuh>
#include <vnegpu/generator/fat_tree.cuh>
#include <vnegpu/metrics.cuh>
#include <vnegpu/algorithm/local_metric.cuh>
#include <vnegpu/algorithm/serial/local_metric.cuh>

BOOST_AUTO_TEST_CASE(Local_metric)
{
  try
  {
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(6);

    int metric_gpu_index = data_center->add_node_variable("GPU Degree");

    int metric_cpu_index = data_center->add_node_variable("CPU Degree");

    data_center->update_gpu();

    vnegpu::algorithm::local_metric(data_center, metric_gpu_index, vnegpu::metrics::degree() );

    data_center->update_cpu();

    vnegpu::algorithm::serial::local_metric(data_center, metric_cpu_index, vnegpu::metrics::degree() );

    for(int id=0; id<data_center->get_num_nodes(); id++)
    {
       float V_GPU = data_center->get_variable_node(metric_gpu_index, id);
       float V_CPU = data_center->get_variable_node(metric_cpu_index, id);
       BOOST_TEST(V_GPU == V_CPU);
    }

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }



}

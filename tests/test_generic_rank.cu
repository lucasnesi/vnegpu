#define BOOST_TEST_MODULE Generic_Rank

#include <boost/test/included/unit_test.hpp>

#include <vnegpu/graph.cuh>
#include <vnegpu/generator/fat_tree.cuh>
#include <vnegpu/metrics.cuh>
#include <vnegpu/algorithm/generic_rank.cuh>
#include <vnegpu/algorithm/serial/generic_rank.cuh>

BOOST_AUTO_TEST_CASE(Generic_Rank)
{
  try
  {
    vnegpu::graph<float>* data_center = vnegpu::generator::fat_tree<float>(6);

    int metric_gpu_index = data_center->add_node_variable("GPU Rank");

    int metric_cpu_index = data_center->add_node_variable("CPU Rank");

    data_center->update_gpu();

    vnegpu::algorithm::generic_rank(data_center, metric_gpu_index, vnegpu::metrics::one() );

    data_center->update_cpu();

    vnegpu::algorithm::serial::generic_rank(data_center, metric_cpu_index, vnegpu::metrics::one() );

    for(int id=0; id<data_center->get_num_nodes(); id++)
    {
       float V_GPU = data_center->get_variable_node(metric_gpu_index, id);
       float V_CPU = data_center->get_variable_node(metric_cpu_index, id);
       BOOST_TEST( fabs(V_GPU-V_CPU) < RANK_MAX_ERROR );
    }

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }



}

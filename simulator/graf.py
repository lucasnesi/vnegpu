#!/usr/bin/python3
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys
import matplotlib
import csv
from pathlib import Path
from matplotlib.ticker import FuncFormatter
import os
import glob
import math
import matplotlib.patches as mpatches
from matplotlib.spines import Spine
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection

font = {'family' : 'Arial',
        'weight' : 'bold',
        'size'   : 16}

matplotlib.rc('font', **font)

modes = ["Modelo1_BestFit", "Modelo1_WorstFit", "Modelo2_BestFit_WorstFit", "Modelo2_WorstFit_BestFit", "Modelo3_BestFit", "Modelo3_WorstFit", "Modelo4_BestFit_WorstFit", "Modelo4_WorstFit_BestFit"]
names = ["1B", "1W", "2BW", "2WB", "3B", "3W", "4BW", "4WB"]

#modes = ["Modelo1_BestFit", "Modelo1_WorstFit", "Modelo2_WorstFit_BestFit", "Modelo4_WorstFit_BestFit"]
#names = ["(a) 1B", "(b) 1W", "(c) 2WB", "(d) 4WB"]

num_linhas = 2
num_colunas = 2
num_files = 20
nomesave = "x_"


class Radar(object):

    def __init__(self, fig, titles, labels, rect=None):
        if rect is None:
            rect = [0.05, 0.08, 0.85, 0.85]

        self.n = len(titles)
        self.angles = np.arange(90, 90+360, 360.0/self.n)
        self.angles = [a % 360 for a in self.angles]
        self.axes = [fig.add_axes(rect, projection="polar", label="axes%d" % i)
                         for i in range(self.n)]
        print(self.axes)

        self.ax = self.axes[0]
        self.ax.grid(linewidth=1)
        self.ax.set_thetagrids(self.angles, labels=titles, fontsize=16)

        for ax in self.axes[1:]:
            ax.patch.set_visible(False)
            ax.grid("off")
            ax.xaxis.set_visible(False)

        for ax, angle, label in zip(self.axes, self.angles, labels):
            ax.set_rgrids(range(1, 6), angle=0, labels=[""*len(titles)], fontsize=6, family='sans-serif', weight='light')
            ax.spines["polar"].set_visible(False)
            ax.set_ylim(0, 5)

        self.ax.set_rgrids(range(1, 6), angle=22, labels=labels[0], fontsize=8, family='sans-serif', weight='normal')
        #self.axes[0].spines["polar"].set_visible(True)

    def plot(self, values, *args, **kw):
        angle = np.deg2rad(np.r_[self.angles, self.angles[0]])
        values = np.r_[values, values[0]]
        self.ax.plot(angle, values, *args, **kw)

    def fill(self, values, *args, **kw):
        angle = np.deg2rad(np.r_[self.angles, self.angles[0]])
        values = np.r_[values, values[0]]
        self.ax.fill(angle, values, *args, **kw)


def pre(f):
    return "%0.2f" % f

def pre_u(f):
    return "%0.2f" % (f/1000)

def ler(file):
    fragmentacao_hosts = np.asarray([])
    fragmentacao_enlaces = np.asarray([])

    fingerprint_hosts_cpu = np.asarray([])
    fingerprint_hosts_mem = np.asarray([])
    fingerprint_switchs = np.asarray([])

    percent_host_0 = np.asarray([])
    percent_host_20 = np.asarray([])
    percent_host_40 = np.asarray([])
    percent_host_60 = np.asarray([])
    percent_host_80 = np.asarray([])
    percent_host_100 = np.asarray([])

    percent_link_0 = np.asarray([])
    percent_link_20 = np.asarray([])
    percent_link_40 = np.asarray([])
    percent_link_60 = np.asarray([])
    percent_link_80 = np.asarray([])
    percent_link_100 = np.asarray([])

    with open(file, 'r') as csvfile:
        spamreader = list(csv.reader(csvfile, delimiter=';'))
        if(len(spamreader)==0):
            return
        for row in spamreader[:100]:
            fragmentacao_hosts = np.append(fragmentacao_hosts, float(row[0]))
            fragmentacao_enlaces = np.append(fragmentacao_enlaces, float(row[1]))
            fingerprint_hosts_cpu = np.append(fingerprint_hosts_cpu, float(row[2]))
            fingerprint_hosts_mem = np.append(fingerprint_hosts_mem, float(row[3]))
            fingerprint_switchs = np.append(fingerprint_switchs, float(row[4]))
            if(len(row)>5):
                percent_host_0 = np.append(percent_host_0, float(row[5]))
                percent_host_20 = np.append(percent_host_20, float(row[6]))
                percent_host_40 = np.append(percent_host_40, float(row[7]))
                percent_host_60 = np.append(percent_host_60, float(row[8]))
                percent_host_80 = np.append(percent_host_80, float(row[9]))
                percent_host_100 = np.append(percent_host_100, float(row[10]))
                percent_link_0 = np.append(percent_link_0, float(row[11]))
                percent_link_20 = np.append(percent_link_20, float(row[12]))
                percent_link_40 = np.append(percent_link_40, float(row[13]))
                percent_link_60 = np.append(percent_link_60, float(row[14]))
                percent_link_80 = np.append(percent_link_80, float(row[15]))
                percent_link_100 = np.append(percent_link_100, float(row[16]))

        accepted = int(spamreader[-1][0])
        time = float(spamreader[-1][1])
        vcpu = float(spamreader[-1][2])
        vcpu_clock = float(spamreader[-1][3])
        node_fail = float(spamreader[-1][4])
        edge_fail = float(spamreader[-1][5])
        lucro=0
        custo=0
        nodes_active_per_clock = 0
        edges_active_per_clock = 0
        dc_hosts = 0
        dc_edges = 0
        dc_band = 0
        clock = 0
        node_2_size = 0
        node_4_size = 0
        if(len(spamreader[-1])>6):
            lucro = float(spamreader[-1][6])
            custo = float(spamreader[-1][7])
        if(len(spamreader[-1])>8):
            nodes_active_per_clock = float(spamreader[-1][8])
            edges_active_per_clock = float(spamreader[-1][9])
            dc_hosts = float(spamreader[-1][10])
            dc_edges = float(spamreader[-1][11])
            dc_band = float(spamreader[-1][12])
            clock = float(spamreader[-1][13])
            node_2_size = float(spamreader[-1][14])
            node_4_size = float(spamreader[-1][15])

    return fragmentacao_hosts, fragmentacao_enlaces, fingerprint_hosts_cpu, fingerprint_hosts_mem, fingerprint_switchs,\
    percent_host_0, percent_host_20, percent_host_40, percent_host_60, percent_host_80, percent_host_100, percent_link_0, percent_link_20, percent_link_40,\
    percent_link_60, percent_link_80, percent_link_100, accepted, time, vcpu, vcpu_clock, node_fail, edge_fail, lucro, custo, nodes_active_per_clock, edges_active_per_clock,\
    dc_hosts, dc_edges, dc_band, clock, node_2_size, node_4_size


def ler_times(file):
    fragmentacao_hosts = np.asarray([])
    fragmentacao_enlaces = np.asarray([])


    with open(file, 'r') as csvfile:
        spamreader = list(csv.reader(csvfile, delimiter=';'))
        if(len(spamreader)==0):
            return
        for row in spamreader:
            fragmentacao_hosts = np.append(fragmentacao_hosts, float(row[0]))
            fragmentacao_enlaces = np.append(fragmentacao_enlaces, float(row[1]))

    return fragmentacao_hosts, fragmentacao_enlaces


def f_numpy(names, values):
    result_names = np.unique(names)
    result_values = np.empty(result_names.shape)

    for i, name in enumerate(result_names):
        result_values[i] = np.mean(values[names == name])

    return result_names, result_values

def draw(ax1, mode, number, a1, a2, a3, a4, a5, a6, acp, time):

    mode_str = "frag"

    #ax1.set_title("Aceitos: "+str(accepted)+" Time:"+ str(time), fontsize=12)
    t=np.arange(0, 100, 1)
    if(mode==0):
        ax1.plot(t,np.mean(a1[number], axis=0),"r-",label="Frag Hosts",linewidth=2)
        ax1.fill_between(t, np.mean(a1[number], axis=0)-np.std(a1[number], axis=0), np.mean(a1[number], axis=0)+np.std(a1[number], axis=0), facecolor='r', alpha=0.25, edgecolor='none')
        ax1.plot(t,np.mean(a2[number], axis=0),"b-",label="Frag Enlaces",linewidth=2)
        ax1.fill_between(t, np.mean(a2[number], axis=0)-np.std(a2[number], axis=0), np.mean(a2[number], axis=0)+np.std(a2[number], axis=0), facecolor='b', alpha=0.25, edgecolor='none')
        #ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10), ncol=3, fancybox=True, shadow=True, fontsize=12)
        ax1.set_ylabel('Fragmentacao', fontsize=10)
        ax1.set_xlabel('Clock', fontsize=10, labelpad=-10)
        ax1.set_xlim([0,100])
        ax1.set_ylim([-0.001,1.01])
        #ax1.set_xticks(np.arange(0, 100, 10))
        ax1.set_yticks(np.arange(0, 1.01, 0.1))
        ax1.grid()
    elif(mode==1):
        ax1.plot(t,np.mean(a1[number], axis=0),"g-",label="Finger CPU",linewidth=2)
        ax1.plot(t,np.mean(a2[number], axis=0),"y-",label="Finger RAM",linewidth=2)
        ax1.plot(t,np.mean(a3[number], axis=0),"c-",label="Finger Banda",linewidth=2)
        ax1.fill_between(t, np.mean(a1[number], axis=0)-np.std(a1[number], axis=0), np.mean(a1[number], axis=0)+np.std(a1[number], axis=0), facecolor='g', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a2[number], axis=0)-np.std(a2[number], axis=0), np.mean(a2[number], axis=0)+np.std(a2[number], axis=0), facecolor='y', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a3[number], axis=0)-np.std(a3[number], axis=0), np.mean(a3[number], axis=0)+np.std(a3[number], axis=0), facecolor='c', alpha=0.25, edgecolor='none')
        #ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10), ncol=3, fancybox=True, shadow=True, fontsize=12)
        ax1.set_ylabel('Fingerprint', fontsize=10)
        ax1.set_xlabel('Clock', fontsize=10, labelpad=-10)
        ax1.set_xlim([0,100])
        ax1.set_ylim([-0.001,1.01])
        #ax1.set_xticks(np.arange(0, 100, 10))
        ax1.set_yticks(np.arange(0, 1.01, 0.1))
        ax1.grid()
    elif(mode==2):
        af1 = np.append([], a1[number])
        af2 = np.append([], a2[number])
        ax1.plot(af2, af1, "g.",label="CPU",markersize=2)
        #ax1.plot(a1[:100], fingerprint_hosts_mem[:100],"y.",label="MEM",markersize=2)
        #ax1.plot(fragmentacao_enlaces[:100], fingerprint_switchs[:100],"b.",label="Band",markersize=2)
        #ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10), ncol=3, fancybox=True, shadow=True, fontsize=12)
        ax1.set_ylabel('Fingerprint', fontsize=10)
        ax1.set_xlabel('Fragmen', fontsize=10, labelpad=-10)
        ax1.set_xlim([-0.01,1.01])
        ax1.set_ylim([-0.001,1.01])
        #ax1.set_xticks(np.arange(0, 1, 0.1))
        ax1.set_yticks(np.arange(0, 1.01, 0.1))
        ax1.grid()
    elif(mode==3):
        ax1.plot(t,np.mean(a1[number], axis=0),"m-",label="0%",linewidth=2)
        ax1.plot(t,np.mean(a2[number], axis=0),"g-",label="0%-20%",linewidth=2)
        ax1.plot(t,np.mean(a3[number], axis=0),"y-",label="20%-40%",linewidth=2)
        ax1.plot(t,np.mean(a4[number], axis=0),"c-",label="40%-60%",linewidth=2)
        ax1.plot(t,np.mean(a5[number], axis=0),"r-",label="60%-80%",linewidth=2)
        ax1.plot(t,np.mean(a6[number], axis=0),"b-",label="80%+",linewidth=2)
        ax1.fill_between(t, np.mean(a1[number], axis=0)-np.std(a1[number], axis=0), np.mean(a1[number], axis=0)+np.std(a1[number], axis=0), facecolor='m', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a2[number], axis=0)-np.std(a2[number], axis=0), np.mean(a2[number], axis=0)+np.std(a2[number], axis=0), facecolor='g', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a3[number], axis=0)-np.std(a3[number], axis=0), np.mean(a3[number], axis=0)+np.std(a3[number], axis=0), facecolor='y', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a4[number], axis=0)-np.std(a4[number], axis=0), np.mean(a4[number], axis=0)+np.std(a4[number], axis=0), facecolor='c', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a5[number], axis=0)-np.std(a5[number], axis=0), np.mean(a5[number], axis=0)+np.std(a5[number], axis=0), facecolor='r', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a6[number], axis=0)-np.std(a6[number], axis=0), np.mean(a6[number], axis=0)+np.std(a6[number], axis=0), facecolor='b', alpha=0.25, edgecolor='none')
        #ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10), ncol=3, fancybox=True, shadow=True, fontsize=12)
        ax1.set_ylabel('Utilização Hosts', fontsize=14)
        ax1.set_xlabel('Intervalo', fontsize=14, labelpad=-10)
        ax1.set_xlim([0,100])
        ax1.set_ylim([-0.001,1.01])
        #ax1.set_xticks(np.arange(0, 100, 10))
        ax1.set_yticks(np.arange(0, 1.01, 0.1))
        ax1.grid()
    elif(mode==4):
        ax1.plot(t,np.mean(a1[number], axis=0),"m-",label="0%",linewidth=2)
        ax1.plot(t,np.mean(a2[number], axis=0),"g-",label="0%-20%",linewidth=2)
        ax1.plot(t,np.mean(a3[number], axis=0),"y-",label="20%-40%",linewidth=2)
        ax1.plot(t,np.mean(a4[number], axis=0),"c-",label="40%-60%",linewidth=2)
        ax1.plot(t,np.mean(a5[number], axis=0),"r-",label="60%-80%",linewidth=2)
        ax1.plot(t,np.mean(a6[number], axis=0),"b-",label="80%+",linewidth=2)
        ax1.fill_between(t, np.mean(a1[number], axis=0)-np.std(a1[number], axis=0), np.mean(a1[number], axis=0)+np.std(a1[number], axis=0), facecolor='m', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a2[number], axis=0)-np.std(a2[number], axis=0), np.mean(a2[number], axis=0)+np.std(a2[number], axis=0), facecolor='g', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a3[number], axis=0)-np.std(a3[number], axis=0), np.mean(a3[number], axis=0)+np.std(a3[number], axis=0), facecolor='y', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a4[number], axis=0)-np.std(a4[number], axis=0), np.mean(a4[number], axis=0)+np.std(a4[number], axis=0), facecolor='c', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a5[number], axis=0)-np.std(a5[number], axis=0), np.mean(a5[number], axis=0)+np.std(a5[number], axis=0), facecolor='r', alpha=0.25, edgecolor='none')
        ax1.fill_between(t, np.mean(a6[number], axis=0)-np.std(a6[number], axis=0), np.mean(a6[number], axis=0)+np.std(a6[number], axis=0), facecolor='b', alpha=0.25, edgecolor='none')
        #ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10), ncol=3, fancybox=True, shadow=True, fontsize=12)
        ax1.set_ylabel('Utilização Enlaces', fontsize=14)
        ax1.set_xlabel('Intervalo', fontsize=14, labelpad=-10)
        ax1.set_xlim([0,100])
        ax1.set_ylim([-0.001,1.01])
        #ax1.set_xticks(np.arange(0, 100, 10))
        ax1.set_yticks(np.arange(0, 1.01, 0.1))
        ax1.grid()
    elif(mode==5):
        values = np.array(a1[number])
        namesa = np.array(a2[number])
        #print(str(len(a1)) + " - " + str(len(a2)))
        af2, af1 = f_numpy(namesa, values)

        ax1.plot(af2, af1, "g.",label="Ponto",markersize=5)
        #ax1.plot(a1[:100], fingerprint_hosts_mem[:100],"y.",label="MEM",markersize=2)
        #ax1.plot(fragmentacao_enlaces[:100], fingerprint_switchs[:100],"b.",label="Band",markersize=2)
        #ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10), ncol=3, fancybox=True, shadow=True, fontsize=12)
        ax1.set_ylabel('Tempo', fontsize=14)
        ax1.set_xlabel('Tamanho', fontsize=14)
        #ax1.set_xlim([-0.01,1.01])
        ax1.set_ylim([0,250])
        ax1.set_yticks(np.arange(0, 275, 25))
        #ax1.set_yticks(np.arange(0, 1.01, 0.1))
        ax1.grid()


    #ax1.set_ylim([0,max(fragmentacao_hosts)+5])
    #ax2.set_ylim([0.90,1.0])

    ax1.set_title(names[number], fontsize=14)
    #ax1.set_title("Aceitos: " + str(np.mean(acp[number])) + "  std:" + pre(np.std(acp[number])), fontsize=8, loc='left')
    #ax1.set_title("Tempo: " + pre_u(np.mean(time[number])) + "s std:" + pre_u(np.std(time[number])), fontsize=8, loc='right')


    ax1.tick_params(axis='both', which='minor', labelsize=8)
    #fig.suptitle(file[2:-4]+"_"+mode_str, fontsize=14, fontweight='bold')

cores = ['m', 'g', 'y', 'c', 'r', 'b', "orange", 'w']

cores_8 = ['m', 'g', 'r', 'y', 'w', 'b', "orange", 'c']


def max10(x):
    return math.ceil( (np.amax(x)*1.1) / 1000.0) * 1000.0


def millions(x, pos):
    return '%1.1fM' % (x*1e-6)


def fast_graf_combo_bar_ax(ax, nome, x_name, y_name, data, scale, lim_i, lim_u, form=None):
    error_config = {'ecolor': '0.3'}
    for i in range(len(modes)):
        ax.bar(i, np.mean(data[i])/scale, 0.9,
                 alpha=0.6,
                 color=cores[i],
                 yerr=np.std(data[i])/scale,
                 error_kw=error_config,
                 label=names[i])

    ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid()
    ax.set_ylabel(y_name, fontsize=10)
    ax.set_xlabel(x_name, fontsize=8)
    ax.set_xticklabels(names)
    if form is not None:
        formatter = FuncFormatter(form)
        ax.yaxis.set_major_formatter(formatter)
    #ax.set_title('Acceptance ratio')
    ax.set_ylim([lim_i,lim_u])


def fast_graf_combo_staked_bar_ax(ax, nome, x_name, y_name, data, data2, scale, lim_i, lim_u, form=None):
    error_config = {'ecolor': '0.3'}
    print(data)
    data1 = np.mean(data[0])/scale
    for i in range(len(modes)):
        ax.bar(i, np.mean(data[i])/scale, 0.9,
                 alpha=0.6,
                 color='#d62728',
                 yerr=np.std(data[i])/scale,
                 error_kw=error_config,
                 label=names[i])
        ax.bar(i, np.mean(data2[i])/scale, 0.9,
                 alpha=0.6,
                 color='#2672d6',
                 yerr=np.std(data2[i])/scale,
                 error_kw=error_config,
                 label=names[i],
                 bottom=np.mean(data[i])/scale)

    ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid()
    ax.set_ylabel(y_name, fontsize=10)
    ax.set_xlabel(x_name, fontsize=8)
    ax.set_xticklabels(names)
    if form is not None:
        formatter = FuncFormatter(form)
        ax.yaxis.set_major_formatter(formatter)
    #ax.set_title('Acceptance ratio')
    ax.set_ylim([lim_i,lim_u])




def fast_graf_combo_bar(nome, x_name, y_name, data, scale, lim_i, lim_u, form=None):
    legen = []
    width = 1
    opacity = 0.6
    for i in range(len(modes)):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=cores_8[i], alpha=opacity, label=''))
    error_config = {'ecolor': '0.9'}

    f, ax = plt.subplots(figsize=(10, 5))

    for i in range(len(modes)):
        ax.bar(i, np.mean(data[i])/scale, 1,
                 alpha=1.0,
                 color=cores_8[i],
                 yerr=np.std(data[i])/scale,
                 error_kw=error_config,
                 label=names[i])

    #ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid(color='#d5d5d5')
    ax.set_ylabel(y_name, fontsize=20, color='#e9e9e9', weight='bold')
    ax.set_xlabel(x_name, fontsize=20, color='#e9e9e9', weight='bold')
    ax.tick_params(axis='x', colors='#d5d5d5')
    ax.tick_params(axis='y', colors='#d5d5d5')
    ax.yaxis.label.set_color('#d5d5d5')
    ax.xaxis.label.set_color('#d5d5d5')
    ax.spines['bottom'].set_color('#d5d5d5')
    ax.spines['top'].set_color('#d5d5d5')
    ax.spines['right'].set_color('#d5d5d5')
    ax.spines['left'].set_color('#d5d5d5')

    #ax.set_xticklabels(names)
    ax.set_xticks([])
    if form is not None:
        formatter = FuncFormatter(form)
        ax.yaxis.set_major_formatter(formatter)
    #ax.set_title('Acceptance ratio')
    ax.set_ylim([lim_i,lim_u])
    #
    lgd = ax.legend(handles=legen, labels=names, loc='lower center', ncol=8, fontsize=11, bbox_to_anchor=(0.5, -0.2))
    f.savefig(nomesave + nome + ".pdf", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight', transparent=True)
    plt.close()


def fast_graf_combo_dots_ax(ax, nome, x_name, y_name, data_x, data_y, lim_i=None, lim_u=None, size=1, line="None"):
    opacity = 0.6

    for i in range(len(modes)):
        ax.plot(data_x[i],data_y[i],color=cores[i], linestyle=line, marker='.',label=names[i],markersize=size, linewidth=1, alpha=opacity)

    #ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid()
    if(y_name!=None):
        ax.set_ylabel(y_name, fontsize=12)

    if(x_name!=None):
        ax.set_xlabel(x_name, fontsize=12)

    if(nome!=None):
        ax.set_title(nome, fontsize=14, y=1.03)


def fast_graf_combo_dots(nome, x_name, y_name, data_x, data_y, lim_i=None, lim_u=None, size=1, line="None"):
    legen = []
    width = 1
    opacity = 0.6
    for i in range(len(modes)):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=cores[i], alpha=opacity, label=''))

    f, ax = plt.subplots(figsize=(10, 5))

    for i in range(len(modes)):
        ax.plot(data_x[i],data_y[i],color=cores[i], linestyle=line, marker='.',label=names[i],markersize=size, linewidth=1, alpha=opacity)

    #ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid()
    ax.set_ylabel(y_name, fontsize=14)
    ax.set_xlabel(x_name, fontsize=14)
    #ax.set_xticklabels(names)
    #ax.set_title('Acceptance ratio')
    ax.set_ylim([-0.01,1.01])
    #
    lgd = ax.legend(handles=legen, labels=names, loc='center right', ncol=8, fontsize=11, bbox_to_anchor=(0.75, -0.2))
    f.savefig(nomesave + nome + ".jpg", dpi=300, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


def draw_info(plt, aceitos, vcpus_allo, vcpus_clock, node_fail, edge_fail, sizes):

    legen = []
    width = 1
    opacity = 0.6
    for i in range(len(modes)):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=cores[i], alpha=opacity, label=''))
    error_config = {'ecolor': '0.3'}


    f, axarr = plt.subplots(2, 3)
    f.set_figheight(9)
    f.set_figwidth(20)


    times_bars = []
    tick = 10

    print(node_fail)

    for i in range(len(modes)):
        times_bars.append(axarr[0, 0].bar(i, np.mean(aceitos[i]), width,
                 alpha=opacity,
                 color=cores[i],
                 yerr=np.std(aceitos[i]),
                 error_kw=error_config,
                 label=modes[i]))

    axarr[0, 0].set_xticks(np.arange(0, len(modes), 1))
    axarr[0, 0].set_title("Aceitos", fontsize=10, y=1.03)
    axarr[0, 0].yaxis.grid()
    axarr[0, 0].set_ylabel('IVs Aceitas', fontsize=10)
    axarr[0, 0].set_xlabel('Modelo', fontsize=10, labelpad=-10)
    #axarr[0, 0].set_yticks(np.arange(0, max10(aceitos), max10(aceitos)/20))

    for i in range(len(modes)):
        times_bars.append(axarr[0, 1].bar(i, np.mean(vcpus_allo[i]), width,
                 alpha=opacity,
                 color=cores[i],
                 yerr=np.std(vcpus_allo[i]),
                 error_kw=error_config,
                 label=modes[i]))

    axarr[0, 1].set_xticks(np.arange(0, len(modes), 1))
    axarr[0, 1].set_title("vCPUs alocadas", fontsize=10, y=1.03)
    axarr[0, 1].yaxis.grid()
    axarr[0, 1].set_ylim([np.amin(vcpus_allo)*0.9,np.amax(vcpus_allo)*1.1])
    axarr[0, 1].set_ylabel('vCPUs alocadas', fontsize=10)
    axarr[0, 1].set_xlabel('Modelo', fontsize=10, labelpad=-10)
    #axarr[0, 1].set_yticks(np.arange(0, max10(vcpus_allo), max10(vcpus_allo)/20))


    w=0.5
    ind = np.arange(len(modes)+1)
    for i in range(len(modes)):
        unique, counts = np.unique(sizes[i], return_counts=True)
        axarr[0, 2].plot(unique,counts,color=cores[i], linestyle="None", marker='.',label="",linewidth=2,alpha=opacity)
        '''axarr[0, 2].bar(unique+i*w, counts, w,
                 alpha=opacity,
                 color=cores[i],
                 error_kw=error_config,
                 label=modes[i])'''

    axarr[0, 2].set_title("IV size", fontsize=10, y=1.03)
    axarr[0, 2].grid()
    axarr[0, 2].set_ylabel('IVs Aceitas', fontsize=10)
    axarr[0, 2].set_xlabel('Tamanho IV', fontsize=10, labelpad=0)


    for i in range(len(modes)):
        times_bars.append(axarr[1, 0].bar(i, np.mean(vcpus_clock[i]), width,
                 alpha=opacity,
                 color=cores[i],
                 yerr=np.std(vcpus_clock[i]),
                 error_kw=error_config,
                 label=modes[i]))

    axarr[1, 0].set_xticks(np.arange(0, len(modes), 1))
    axarr[1, 0].set_title("vCPUs * Clock", fontsize=10, y=1.03)
    axarr[1, 0].yaxis.grid()
    axarr[1, 0].set_ylim([np.amin(vcpus_clock)*0.9,np.amax(vcpus_clock)*1.1])
    axarr[1, 0].set_ylabel('Hora Uso', fontsize=10)
    axarr[1, 0].set_xlabel('Modelo', fontsize=10, labelpad=-10)

    for i in range(len(modes)):
        times_bars.append(axarr[1, 1].bar(i, np.mean(node_fail[i]), width,
                 alpha=opacity,
                 color=cores[i],
                 yerr=np.std(node_fail[i]),
                 error_kw=error_config,
                 label=modes[i]))

    axarr[1, 1].set_xticks(np.arange(0, len(modes), 1))
    axarr[1, 1].set_title("Node Fail", fontsize=10, y=1.03)
    axarr[1, 1].yaxis.grid()
    axarr[1, 1].set_ylabel('Numero Falhas', fontsize=10)
    axarr[1, 1].set_xlabel('Modelo', fontsize=10, labelpad=-10)

    for i in range(len(modes)):
        times_bars.append(axarr[1, 2].bar(i, np.mean(edge_fail[i]), width,
                 alpha=opacity,
                 color=cores[i],
                 yerr=np.std(edge_fail[i]),
                 error_kw=error_config,
                 label=modes[i]))

    axarr[1, 2].set_xticks(np.arange(0, len(modes), 1))
    axarr[1, 2].set_title("Edge Fail", fontsize=10, y=1.03)
    axarr[1, 2].yaxis.grid()
    axarr[1, 2].set_ylabel('Numero Falhas', fontsize=10)
    axarr[1, 2].set_xlabel('Modelo', fontsize=10, labelpad=-10)

    #axarr[1, 0].set_yticks(np.arange(0, max10(vcpus_clock), max10(vcpus_clock)/20))



    plt.figlegend(handles=legen, labels=modes, loc='lower center', ncol=3, fancybox=True, shadow=True, fontsize=10)
    f.suptitle('Info')
    f.savefig(nomesave + "info.jpg", dpi=200, bbox_inches='tight')
    plt.close()

def radar_norm(data, i):
    means = np.mean(data, axis=1)
    if(means[i]==0):
        return 0
    return means[i]/np.amax(means)*5

def main(argv):
    folder = "./"
    if(len(argv)>0):
        folder = folder+argv[0]+"/"

    modo_num = 0

    global nomesave
    nomesave = argv[0]+"_"

    path = folder+"Seed*" + modes[0] + ".csv"

    global num_files
    num_files = len(sorted(glob.glob(path)))

    print(num_files)

    num_modos = len(modes)

    fragmentacao_hosts = np.zeros((num_modos, num_files, 100))
    fragmentacao_enlaces = np.zeros((num_modos, num_files, 100))
    finger_cpu = np.zeros((num_modos, num_files, 100))
    finger_mem = np.zeros((num_modos, num_files, 100))
    finger_band = np.zeros((num_modos, num_files, 100))
    util_hosts_0 = np.zeros((num_modos, num_files, 100))
    util_hosts_20 = np.zeros((num_modos, num_files, 100))
    util_hosts_40 = np.zeros((num_modos, num_files, 100))
    util_hosts_60 = np.zeros((num_modos, num_files, 100))
    util_hosts_80 = np.zeros((num_modos, num_files, 100))
    util_hosts_100 = np.zeros((num_modos, num_files, 100))
    util_enlaces_0 = np.zeros((num_modos, num_files, 100))
    util_enlaces_20 = np.zeros((num_modos, num_files, 100))
    util_enlaces_40 = np.zeros((num_modos, num_files, 100))
    util_enlaces_60 = np.zeros((num_modos, num_files, 100))
    util_enlaces_80 = np.zeros((num_modos, num_files, 100))
    util_enlaces_100 = np.zeros((num_modos, num_files, 100))

    aloc_tempos = []
    aloc_size = []

    for i in range(num_modos):
        aloc_tempos.append([])
        aloc_size.append([])

    aceitos = np.zeros((num_modos, num_files))
    tempo = np.zeros((num_modos, num_files))
    vcpus = np.zeros((num_modos, num_files))
    vcpus_clock = np.zeros((num_modos, num_files))
    node_fail = np.zeros((num_modos, num_files))
    edge_fail = np.zeros((num_modos, num_files))
    lucro = np.zeros((num_modos, num_files))
    custo = np.zeros((num_modos, num_files))
    nodes_active_per_clock = np.zeros((num_modos, num_files))
    edges_active_per_clock = np.zeros((num_modos, num_files))
    dc_hosts = np.zeros((num_modos, num_files))
    dc_edges = np.zeros((num_modos, num_files))
    dc_band = np.zeros((num_modos, num_files))
    clock = np.zeros((num_modos, num_files))
    node_2_size = np.zeros((num_modos, num_files))
    node_4_size = np.zeros((num_modos, num_files))


    for mod in modes:

        path = folder+"*" + mod + ".csv"
        print(path)
        seed = 0
        for file in sorted(glob.glob(path)):
            print(file)
            fh, fe, fc, fm, fs, ph0, ph2, ph4, ph6, ph8, ph10, pl0, pl2, pl4, pl6, pl8, pl10, ac, ti, vc, vcc, nf, ef, lu, cu, nc, ne, dh, de, db, c, n2, n4 = ler(file)
            fragmentacao_hosts[modo_num][seed] = fh
            fragmentacao_enlaces[modo_num][seed] = fe
            finger_cpu[modo_num][seed] = fc
            finger_mem[modo_num][seed] = fm
            finger_band[modo_num][seed] = fs
            util_hosts_0[modo_num][seed] = ph0
            util_hosts_20[modo_num][seed] = ph2
            util_hosts_40[modo_num][seed] = ph4
            util_hosts_60[modo_num][seed] = ph6
            util_hosts_80[modo_num][seed] = ph8
            util_hosts_100[modo_num][seed] = ph10
            util_enlaces_0[modo_num][seed] = pl0
            util_enlaces_20[modo_num][seed] = pl2
            util_enlaces_40[modo_num][seed] = pl4
            util_enlaces_60[modo_num][seed] = pl6
            util_enlaces_80[modo_num][seed] = pl8
            util_enlaces_100[modo_num][seed] = pl10
            aceitos[modo_num][seed] = ac
            tempo[modo_num][seed] = ti
            vcpus[modo_num][seed] = vc
            vcpus_clock[modo_num][seed] = vcc
            node_fail[modo_num][seed] = nf
            edge_fail[modo_num][seed] = ef
            lucro[modo_num][seed] = lu
            custo[modo_num][seed] = cu
            nodes_active_per_clock[modo_num][seed] = nc
            edges_active_per_clock[modo_num][seed] = ne
            dc_hosts[modo_num][seed] = dh
            dc_edges[modo_num][seed] = de
            dc_band[modo_num][seed] = db
            clock[modo_num][seed] = c
            node_2_size[modo_num][seed] = n2
            node_4_size[modo_num][seed] = n4
            seed+=1
            if(seed==num_files):
                break

        modo_num+=1

    modo_num = 0
    for mod in modes:

        path = folder+"*" + mod + ".csv_times"
        seed = 0
        for file in sorted(glob.glob(path)):
            print(file)
            at, ass = ler_times(file)

            aloc_tempos[modo_num].extend(at)
            aloc_size[modo_num].extend(ass)

            seed+=1
            if(seed==num_files):
                break
        modo_num+=1

    #fig = plt.figure(figsize=(12,8))
    f, axarr = plt.subplots(num_linhas, num_colunas)
    f.set_figheight(9)
    f.set_figwidth(16)
    for i, ax in enumerate(f.axes):
        if i>=num_modos:
            break;
        draw(ax, 0, i, fragmentacao_hosts, fragmentacao_enlaces, 0, 0, 0, 0, aceitos, tempo)
    #draw(axarr[0, 1], 0, 1, fragmentacao_hosts, fragmentacao_enlaces, 0, 0, 0, 0, aceitos, tempo)
    #draw(axarr[1, 0], 0, 2, fragmentacao_hosts, fragmentacao_enlaces, 0, 0, 0, 0, aceitos, tempo)
    #draw(axarr[1, 1], 0, 3, fragmentacao_hosts, fragmentacao_enlaces, 0, 0, 0, 0, aceitos, tempo)
    h1 = plt.Line2D([], [], color='r', marker='_', markersize=15, label='')
    h2 = plt.Line2D([], [], color='b', marker='_', markersize=15, label='')
    plt.figlegend(handles=[h1, h2], labels=["Hosts", "Enlaces"], loc='lower center', ncol=3, fancybox=True, shadow=True, fontsize=10)
    f.suptitle('Fragmentacao')
    f.savefig(nomesave + "Fragmentacao.jpg", dpi=200)
    plt.close()


    f, axarr = plt.subplots(num_linhas, num_colunas)
    f.set_figheight(9)
    f.set_figwidth(16)
    for i, ax in enumerate(f.axes):
        if i>=num_modos:
            break;
        draw(ax, 5, i, aloc_tempos, aloc_size, 0, 0, 0, 0, aceitos, tempo)

    h1 = plt.Line2D([], [], color='g', marker='_', markersize=15, label='')
    #plt.figlegend(handles=[h1], labels=["Ponto"], loc='lower center', ncol=3, fancybox=True, shadow=True, fontsize=10)
    #f.suptitle('Tempo_Request')
    plt.tight_layout(pad=0.4, w_pad=0.2, h_pad=0.2)
    f.savefig(nomesave + "Tempo_Request.jpg", dpi=200, bbox_inches='tight')
    plt.close()


    f, axarr = plt.subplots(num_linhas, num_colunas)
    f.set_figheight(9)
    f.set_figwidth(16)
    for i, ax in enumerate(f.axes):
        if i>=num_modos:
            break;
        draw(ax, 1, i, finger_cpu, finger_mem, finger_band, 0, 0, 0, aceitos, tempo)

    h1 = plt.Line2D([], [], color='g', marker='_', markersize=15, label='')
    h2 = plt.Line2D([], [], color='y', marker='_', markersize=15, label='')
    h3 = plt.Line2D([], [], color='c', marker='_', markersize=15, label='')
    plt.figlegend(handles=[h1, h2, h3], labels=["CPU", "Mem", "Banda"], loc='lower center', ncol=3, fancybox=True, shadow=True, fontsize=10)
    f.suptitle('Fingerprint')
    f.savefig(nomesave + "Fingerprint.jpg", dpi=200, bbox_inches='tight')
    plt.close()

    f, axarr = plt.subplots(num_linhas, num_colunas)
    f.set_figheight(9)
    f.set_figwidth(16)
    for i, ax in enumerate(f.axes):
        if i>=num_modos:
            break;
        draw(ax, 2, i, finger_cpu, fragmentacao_hosts, 0, 0, 0, 0, aceitos, tempo)

    h1 = plt.Line2D([], [], color='g', marker='.', markersize=15, label='')
    plt.figlegend(handles=[h1], labels=["CPU"], loc='lower center', ncol=3, fancybox=True, shadow=True, fontsize=10)
    f.suptitle('Fingerprint x Fragmentacao')
    f.savefig(nomesave + "FingerXFrag.jpg", dpi=200, bbox_inches='tight')
    plt.close()

    f, axarr = plt.subplots(num_linhas, num_colunas)
    f.set_figheight(9)
    f.set_figwidth(16)
    for i, ax in enumerate(f.axes):
        if i>=num_modos:
            break;
        draw(ax, 3, i, util_hosts_0, util_hosts_20, util_hosts_40, util_hosts_60, util_hosts_80, util_hosts_100, aceitos, tempo)

    h1 = plt.Line2D([], [], color='m', marker='_', markersize=15, label='')
    h2 = plt.Line2D([], [], color='g', marker='_', markersize=15, label='')
    h3 = plt.Line2D([], [], color='y', marker='_', markersize=15, label='')
    h4 = plt.Line2D([], [], color='c', marker='_', markersize=15, label='')
    h5 = plt.Line2D([], [], color='r', marker='_', markersize=15, label='')
    h6 = plt.Line2D([], [], color='b', marker='_', markersize=15, label='')
    plt.figlegend(handles=[h1, h2, h3, h4, h5, h6], labels=["0%", "1%-20%", "21%-40%", "41%-60%", "61%-80%", "81%-100%"], loc='lower center', ncol=6, fontsize=14)
    #f.suptitle('Utilizacao Hosts')
    f.savefig(nomesave + "Util_hosts.jpg", dpi=200, bbox_inches='tight')
    plt.close()

    f, axarr = plt.subplots(num_linhas, num_colunas)
    f.set_figheight(9)
    f.set_figwidth(16)
    for i, ax in enumerate(f.axes):
        if i>=num_modos:
            break;
        draw(ax, 4, i, util_enlaces_0, util_enlaces_20, util_enlaces_40, util_enlaces_60, util_enlaces_80, util_enlaces_100, aceitos, tempo)

    h1 = plt.Line2D([], [], color='m', marker='_', markersize=15, label='')
    h2 = plt.Line2D([], [], color='g', marker='_', markersize=15, label='')
    h3 = plt.Line2D([], [], color='y', marker='_', markersize=15, label='')
    h4 = plt.Line2D([], [], color='c', marker='_', markersize=15, label='')
    h5 = plt.Line2D([], [], color='r', marker='_', markersize=15, label='')
    h6 = plt.Line2D([], [], color='b', marker='_', markersize=15, label='')
    plt.figlegend(handles=[h1, h2, h3, h4, h5, h6], labels=["0%", "1%-20%", "21%-40%", "41%-60%", "61%-80%", "81%-100%"], loc='lower center', ncol=6, fontsize=14)
    #f.suptitle('Utilizacao Enlaces')

    f.savefig(nomesave + "Util_enlaces.jpg", dpi=200, bbox_inches='tight')
    plt.close()
    #plt.show()
    #

    draw_info(plt, aceitos, vcpus, vcpus_clock, node_fail, edge_fail, aloc_size)
    fast_graf_combo_bar("Aceita","Modelo","Taxa de Aceitação", aceitos, 3000, -0.0001, 1.0001)
    fast_graf_combo_bar("CPUS","Modelo","Recursos de nós alocados", vcpus, 1, np.amin(vcpus)*0.9, np.amax(vcpus)*1.1, millions)
    unique = []
    counts = []
    for i in range(len(modes)):
        x, y = np.unique(aloc_size[i], return_counts=True)
        unique.append(x)
        counts.append(y)

    fast_graf_combo_dots("IV_Size_Allocated", "IV Size", "Number of Allocations", unique, counts, size=4);

    fast_graf_combo_dots("FxF", "Fragmentação", "Footprint", fragmentacao_hosts, finger_cpu, size=3);
    fast_graf_combo_dots("FxF2", "Fragmentação", "Footprint", fragmentacao_enlaces, finger_band, size=3);


    fig = plt.figure(figsize=(3, 3))

    titles = ["AC","RN","RE","C/L", "2S", "4S", "FN", "FE"]

    labels = [
        [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0]
    ]

    radar = Radar(fig, titles, labels)
    print("a")
    print(lucro)
    c_l = custo/lucro

    pre_data = []

    for i in range(len(modes)):
        pre_data = [radar_norm(aceitos, i), radar_norm(node_fail, i), radar_norm(edge_fail, i), radar_norm(c_l, i),  radar_norm(node_2_size, i),  radar_norm(node_4_size, i),  radar_norm(nodes_active_per_clock, i),  radar_norm(edges_active_per_clock, i)];
        print(pre_data)
        radar.plot(pre_data,  "-", lw=2, color=cores[i], alpha=0.7, label=names[i])
        radar.fill(pre_data,  "-", lw=2, color=cores[i], alpha=0.05)
        #radar.plot([np.mean(aceitos[i])/3000*5, np.mean(node_fail[i])/3000*10, np.mean(edge_fail[i])/3000*10,  np.mean(custo[i])/ np.mean(lucro[i])/2],  "-", lw=2, color=cores[i], alpha=0.7, label=names[i])

    #radar.ax.legend()

    opacity = 0.6
    legen = []
    for i in range(len(modes)):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=cores[i], alpha=opacity, label=''))

    lgd = plt.legend(handles=legen, labels=names, loc='lower center', ncol=len(modes), fontsize=8, bbox_to_anchor=(0.5, -0.2))
    fig.savefig(nomesave + "radar.jpg", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()



    #rad2(6, pre_data, titles);

    f, axarr = plt.subplots(1, 2)
    f.set_figheight(2)
    f.set_figwidth(6)
    fast_graf_combo_bar_ax(axarr[0], "Aceita","Model","Acceptance Ratio", aceitos, 3000, -0.0001, 1.0001)
    fast_graf_combo_bar_ax(axarr[1], "CPUS","Model","Allocated node resources", vcpus, 1, np.amin(vcpus)*0.98, np.amax(vcpus)*1.01, millions)

    opacity = 0.6
    legen = []
    for i in range(len(modes)):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=cores[i], alpha=opacity, label=''))
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    lgd = plt.legend(handles=legen, labels=names, loc='lower center', ncol=len(modes), fontsize=8, bbox_to_anchor=(-0.19, -0.44))
    f.savefig(nomesave + "Aceit.jpg", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight', transparent=True)
    plt.close()



    f, axarr = plt.subplots(1, 3)
    f.set_figheight(2)
    f.set_figwidth(6)
    fast_graf_combo_staked_bar_ax(axarr[0], "Aceita","Model","Acceptance Ratio", node_2_size, node_4_size, 3000, -0.0001, 1.0001)
    fast_graf_combo_staked_bar_ax(axarr[1], "Aceita","Model","Acceptance Ratio", node_2_size, node_4_size, 3000, -0.0001, 1.0001)
    fast_graf_combo_staked_bar_ax(axarr[2], "Aceita","Model","Acceptance Ratio", node_2_size, node_4_size, 3000, -0.0001, 1.0001)

    color = ['#d62728', '#2672d6']

    opacity = 0.6
    legen = []
    for i in range(2):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=color[i], alpha=opacity, label=''))
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    lgd = plt.legend(handles=legen, labels=["Medium", "xLarge"], loc='lower center', ncol=len(modes), fontsize=8, bbox_to_anchor=(-0.19, -0.44))
    f.savefig(nomesave + "_sizess.jpg", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()



    f, axarr = plt.subplots(2, 3)
    f.set_figheight(4)
    f.set_figwidth(9)
    fast_graf_combo_dots_ax(axarr[0, 0], "Fat-Tree 40", "Fragmetation", "Fingerprint", fragmentacao_hosts, finger_cpu, size=4)
    fast_graf_combo_dots_ax(axarr[0, 1], "Bcube 7, 4", "Fragmetation", None, fragmentacao_hosts, finger_cpu, size=4)
    fast_graf_combo_dots_ax(axarr[0, 2], "DCell", "Fragmetation", None, fragmentacao_hosts, finger_cpu, size=4)
    fast_graf_combo_dots_ax(axarr[1, 0], None, "Fragmetation", "Fingerprint", fragmentacao_enlaces, finger_band, size=4)
    fast_graf_combo_dots_ax(axarr[1, 1], None, "Fragmetation", None, fragmentacao_enlaces, finger_band, size=4)
    fast_graf_combo_dots_ax(axarr[1, 2], None, "Fragmetation", None, fragmentacao_enlaces, finger_band, size=4)

    trans = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 0].transAxes)
    line = plt.Line2D([0.347, 0.347], [-1.7, 1.2], color='k', alpha=0.3, linewidth=1, transform=trans)
    f.lines.append(line)
    trans2 = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 1].transAxes)
    line2 = plt.Line2D([0.673, 0.673], [-1.7, 1.2], color='k', alpha=0.3, linewidth=1, transform=trans2)
    f.lines.append(line2)

    opacity = 0.6
    legen = []
    for i in range(len(modes)):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=cores[i], alpha=opacity, label=''))
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    lgd = plt.legend(handles=legen, labels=names, loc='lower center', ncol=len(modes), fontsize=12, bbox_to_anchor=(-0.7, -0.7))
    f.savefig(nomesave + "F_F.jpg", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()

    for i in range(len(modes)):
        print(modes[i] + " " + str(np.mean(aloc_tempos[i])) + " " + str(np.std(aloc_tempos[i])) + " " + str(np.amax(aloc_tempos[i])) + " " + str(np.amin(aloc_tempos[i]))  )



if __name__ == "__main__":
	main(sys.argv[1:])

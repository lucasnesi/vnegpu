#include <stdio.h>
#include <random>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <chrono>

#include <vnegpu/graph.cuh>

#include <vnegpu/metrics.cuh>
#include <vnegpu/distance.cuh>
#include <vnegpu/allocation_models.cuh>

#include <vnegpu/algorithm/graph_metric.cuh>

#include <vnegpu/generator/fat_tree.cuh>
#include <vnegpu/generator/bcube.cuh>
#include <vnegpu/generator/dcell.cuh>
#include <vnegpu/generator/request.cuh>
#include <vnegpu/generator/shuffle_topology.cuh> //Fat tree generator

#define MAX_CLOCK 100
#define DURATION_REQUEST_LIMIT 50

struct basic_machine{
  float cpu;
  float memory;
  float max_banding;
};

const basic_machine amazon_instances[3] = {
  //CPU, RAM, Medium probably max banding
  {2.0f, 4.0f, 44.0f}, //Medium
  {2.0f, 8.0f, 65.0f}, //Large
  {4.0f, 16.0f, 86.0f}, //XLarge
};

const int banding_options[2] = {
  3,
  30
};

struct _request{
  int start;
  int duration;
  int tipo;
  bool done=false;
  bool allocated=false;
  vnegpu::graph<float, vnegpu::graph_type::real_tests>* graph;
};

struct timeline{
  int num_requests;
  int* time_requests;
  _request* requests;
  vnegpu::graph<float, vnegpu::graph_type::real_tests>* data_center;
  vnegpu::graph<float, vnegpu::graph_type::real_tests>* base_data_center;

  timeline(int seed){
    std::random_device rd;
    std::mt19937 gen(seed);

    num_requests = 3000;
    time_requests = new int[num_requests];
    requests = new _request[num_requests];

    std::uniform_int_distribution<> dist_start_time_diff(0, MAX_CLOCK);

    std::uniform_int_distribution<> dist_duration(10, DURATION_REQUEST_LIMIT);

    std::uniform_int_distribution<> hosts_types(0, 2);

    std::uniform_int_distribution<> banding_rand(0, 1);

    std::uniform_int_distribution<> level_tree(4, 7);

    //first request always start on 0.
    time_requests[0]=0;
    for(int i=1; i<num_requests; i++){
      time_requests[i]=dist_start_time_diff(gen);
    }

    std::sort(time_requests, time_requests+num_requests);

    for(int i=0; i<num_requests; i++){
      requests[i].start = time_requests[i];
      requests[i].duration = dist_duration(gen);

      int type_host = hosts_types(gen);

      int type_band = banding_rand(gen);

      //requests[i].graph = vnegpu::generator::web_request<float, vnegpu::graph_type::real_tests>(10, 3);
      int n=3;
      //int v[3] = {6, 6, 5};
      int v[3] = {level_tree(gen), level_tree(gen), level_tree(gen)};
      requests[i].graph = vnegpu::generator::request_gen<float, vnegpu::graph_type::real_tests>(n, v);

      vnegpu::generator::shuffle(requests[i].graph, dist_start_time_diff(gen));

      //requests[i].graph = vnegpu::generator::fat_tree<float, vnegpu::graph_type::real_tests>(12);

      requests[i].tipo = amazon_instances[type_host].cpu;

      for(int z=0; z<requests[i].graph->get_num_nodes(); z++){
        if(requests[i].graph->get_node_type(z) == vnegpu::TYPE_HOST){
          requests[i].graph->set_variable_node(requests[i].graph->variables.node_cpu, z, amazon_instances[type_host].cpu);
          requests[i].graph->set_variable_node(requests[i].graph->variables.node_memory, z, amazon_instances[type_host].memory);
        }else{
          requests[i].graph->set_variable_node(requests[i].graph->variables.node_cpu, z, amazon_instances[0].cpu);
          requests[i].graph->set_variable_node(requests[i].graph->variables.node_memory, z, amazon_instances[0].memory);
        }
      }

      for(int z=0; z<requests[i].graph->get_num_edges(); z++){
        requests[i].graph->set_variable_edge_undirected(requests[i].graph->variables.edge_band, z, banding_options[type_band]);
      }

      //requests[i].graph->save_to_gexf("SIMULATOR_REQUEST.gexf");

    }

    //**********************
    //**** DATA CENTER *****
    //**********************
    //data_center = vnegpu::generator::bcube<float, vnegpu::graph_type::real_tests>(7, 4);
    //base_data_center = vnegpu::generator::bcube<float, vnegpu::graph_type::real_tests>(7, 4);

    //data_center = vnegpu::generator::dcell<float, vnegpu::graph_type::real_tests>(11, 2);
    //base_data_center = vnegpu::generator::dcell<float, vnegpu::graph_type::real_tests>(11, 2);

    data_center = vnegpu::generator::fat_tree<float, vnegpu::graph_type::real_tests>(40);
    base_data_center = vnegpu::generator::fat_tree<float, vnegpu::graph_type::real_tests>(40);

    int seed_gen = dist_start_time_diff(gen);
    vnegpu::generator::shuffle(data_center, seed_gen);
    vnegpu::generator::shuffle(base_data_center, seed_gen);

    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_band, i, 1000);
      base_data_center->set_variable_edge_undirected(data_center->variables.edge_band, i, 1000);
    }

    for(int i=0; i<data_center->get_num_nodes(); i++){
      if(data_center->get_node_type(i) == vnegpu::TYPE_HOST){
        data_center->set_variable_node(data_center->variables.node_cpu, i, 20);
        data_center->set_variable_node(data_center->variables.node_memory, i, 256);
        base_data_center->set_variable_node(data_center->variables.node_cpu, i, 20);
        base_data_center->set_variable_node(data_center->variables.node_memory, i, 256);
      }else{
        data_center->set_variable_node(data_center->variables.node_cpu, i, 0);
        data_center->set_variable_node(data_center->variables.node_memory, i, 0);
        base_data_center->set_variable_node(data_center->variables.node_cpu, i, 0);
        base_data_center->set_variable_node(data_center->variables.node_memory, i, 0);

        if(data_center->get_node_type(i) == vnegpu::TYPE_SWITH_CORE){
          for(int x=data_center->get_source_offset(i); x<data_center->get_source_offset(i+1); x++)
          {
            data_center->set_variable_edge(data_center->variables.edge_band, x, 10000);
            base_data_center->set_variable_edge(data_center->variables.edge_band, x, 10000);
          }
        }
      }
    }

  }
};


template <class FitPolice1, class FitPolice2=vnegpu::allocation::worst_fit_basic_machine_group>
void simulation(int seed, std::string file_name_info, vnegpu::allocation::models selected_model, FitPolice1 police1, FitPolice2 police2=vnegpu::allocation::worst_fit_basic_machine_group()){
    timeline simulator(seed);

    int clock = 0;
    int faltando = simulator.num_requests;
    int next_request=0;

    unsigned int accepted = 0;
    unsigned int vCPU_allocated = 0;
    unsigned int vCPU_allocated_clocks = 0;
    unsigned int node_error = 0;
    unsigned int edge_error = 0;

    unsigned int total_band_asked = 0;
    unsigned int total_band_used = 0;
    unsigned int nodes_active_per_clock = 0;
    unsigned int edges_active_per_clock = 0;

    unsigned int node_2_size = 0;
    unsigned int node_4_size = 0;

    int* fragmentation_info = (int*)malloc(sizeof(int)*2);
    float* fingerprint_info = (float*)malloc(sizeof(float)*3);
    int* percent_util = (int*)malloc(sizeof(int)*12);

    simulator.base_data_center->update_gpu();
    simulator.data_center->update_gpu();

    std::ofstream simulation_info;

    std::ofstream fit_times;

    simulation_info.open(file_name_info);
    fit_times.open(file_name_info+"_times");

    float total_info[3];

    int hosts = simulator.data_center->get_hosts();

    vnegpu::algorithm::fragmentation(simulator.base_data_center, simulator.data_center, fragmentation_info);
    vnegpu::algorithm::fingerprint(simulator.base_data_center, simulator.data_center, fingerprint_info);
    vnegpu::algorithm::percent_util(simulator.base_data_center, simulator.data_center, percent_util);


    total_info[0]=fingerprint_info[0];
    total_info[1]=fingerprint_info[1];
    total_info[2]=fingerprint_info[2];

    simulation_info << fragmentation_info[0]/(float)hosts << ";" << fragmentation_info[1]/(float)simulator.data_center->get_num_edges() << ";"
                    << 1.0f-fingerprint_info[0]/total_info[0] << ";" << 1.0f-fingerprint_info[1]/total_info[1] << ";"
                    << 1.0f-fingerprint_info[2]/total_info[2] << ";" << percent_util[0]/(float)hosts << ";" << percent_util[1]/(float)hosts
                    << ";" << percent_util[2]/(float)hosts << ";" << percent_util[3]/(float)hosts << ";" << percent_util[4]/(float)hosts << ";"
                    << percent_util[5]/(float)hosts << ";"
                    << percent_util[6]/(float)simulator.data_center->get_num_edges() << ";" << percent_util[7]/(float)simulator.data_center->get_num_edges()
                    << ";" << percent_util[8]/(float)simulator.data_center->get_num_edges() << ";" << percent_util[9]/(float)simulator.data_center->get_num_edges()
                    << ";" << percent_util[10]/(float)simulator.data_center->get_num_edges() << ";" << percent_util[11]/(float)simulator.data_center->get_num_edges() << "\n";

    float last_band_fingerprint = 10;

    auto start = std::chrono::steady_clock::now();

    printf("\nInicializating: %s\n", file_name_info.c_str());
    while(faltando>0 && clock <= MAX_CLOCK+DURATION_REQUEST_LIMIT){
      printf("CLOCK:%d - Remaining:%d Fingerprint:%f\n",clock, faltando, fingerprint_info[0]);
      int request_check = 0;
      while(request_check < simulator.num_requests && simulator.requests[request_check].allocated){
          if(!simulator.requests[request_check].done &&
              simulator.requests[request_check].start + simulator.requests[request_check].duration == clock)
          {
              printf("\tDesallocating request: %d\n", request_check);
              simulator.requests[request_check].done = true;
              switch(selected_model){
                case vnegpu::allocation::MODEL_1:
                  vnegpu::allocation::desfit_model_1(simulator.data_center, simulator.requests[request_check].graph, police1);
                  break;
                case vnegpu::allocation::MODEL_2:
                  vnegpu::allocation::desfit_model_2(simulator.data_center, simulator.requests[request_check].graph, police1);
                  break;
                case vnegpu::allocation::MODEL_3:
                  vnegpu::allocation::desfit_model_3(simulator.data_center, simulator.requests[request_check].graph, police1);
                  break;
                case vnegpu::allocation::MODEL_4:
                  vnegpu::allocation::desfit_model_4(simulator.data_center, simulator.requests[request_check].graph, police1);
                  break;
              }
              simulator.requests[request_check].graph->free_graph();
              faltando--;
          }
          request_check++;
      }
      while(next_request < simulator.num_requests && simulator.time_requests[next_request]==clock){
        printf("\tAllocating request: %d\n", next_request);
        simulator.requests[next_request].allocated=true;
        vnegpu::fit_return result;
        bool execute_group = false;
        if(fabs(last_band_fingerprint-(fingerprint_info[2]/total_info[2]))>1.05f){
          execute_group = true;
          last_band_fingerprint=fingerprint_info[2]/total_info[2];
        }

        auto fit_start = std::chrono::steady_clock::now();

        switch(selected_model){
          case vnegpu::allocation::MODEL_1:
            result = vnegpu::allocation::fit_model_1(simulator.data_center, simulator.requests[next_request].graph, police1, next_request);
            break;
          case vnegpu::allocation::MODEL_2:
            result = vnegpu::allocation::fit_model_2(simulator.data_center, simulator.requests[next_request].graph, police1, police2, execute_group);
            break;
          case vnegpu::allocation::MODEL_3:
            result = vnegpu::allocation::fit_model_3(simulator.data_center, simulator.requests[next_request].graph, police1);
            break;
          case vnegpu::allocation::MODEL_4:
            result = vnegpu::allocation::fit_model_4(simulator.data_center, simulator.requests[next_request].graph, police1, police2, next_request, execute_group);
            break;
        }

        if(result==vnegpu::FIT_SUCCESS){
          accepted++;
          int vcpus = vnegpu::algorithm::sum_variable_node(simulator.requests[next_request].graph, simulator.requests[next_request].graph->variables.node_cpu);
          vCPU_allocated+=vcpus;
          vCPU_allocated_clocks += vcpus*simulator.requests[next_request].duration;

          if(simulator.requests[next_request].tipo==2){
            node_2_size++;
          }else if(simulator.requests[next_request].tipo==4){
            node_4_size++;
          }

          total_band_asked += vnegpu::algorithm::sum_variable_edge(simulator.requests[next_request].graph, simulator.requests[next_request].graph->variables.edge_band);
          total_band_used += vnegpu::algorithm::sum_variable_edge_used(simulator.requests[next_request].graph, simulator.requests[next_request].graph->variables.edge_band);

          auto end_fit = std::chrono::steady_clock::now();
          auto diff_fit = end_fit - fit_start;
          fit_times << std::chrono::duration <double, std::milli> (diff_fit).count() << ";" << simulator.requests[next_request].graph->get_num_nodes() << "\n";

        }else{
          if(result==vnegpu::FIT_NODE_ERROR){
            node_error++;
          }else if(result==vnegpu::FIT_EDGE_ERROR){
            edge_error++;
          }
          simulator.requests[next_request].done = true;
          simulator.requests[next_request].graph->free_graph();
          faltando--;
        }
        next_request++;
      }

      simulator.data_center->update_gpu();

      vnegpu::algorithm::fragmentation(simulator.base_data_center, simulator.data_center, fragmentation_info);
      vnegpu::algorithm::fingerprint(simulator.base_data_center, simulator.data_center, fingerprint_info);
      vnegpu::algorithm::percent_util(simulator.base_data_center, simulator.data_center, percent_util);

      nodes_active_per_clock+=fragmentation_info[0];
      edges_active_per_clock+=fragmentation_info[1];

      simulation_info << fragmentation_info[0]/(float)hosts << ";" << fragmentation_info[1]/(float)simulator.data_center->get_num_edges() << ";"
                      << 1.0f-fingerprint_info[0]/total_info[0] << ";" << 1.0f-fingerprint_info[1]/total_info[1] << ";"
                      << 1.0f-fingerprint_info[2]/total_info[2] << ";" << percent_util[0]/(float)hosts << ";" << percent_util[1]/(float)hosts
                      << ";" << percent_util[2]/(float)hosts << ";" << percent_util[3]/(float)hosts << ";" << percent_util[4]/(float)hosts << ";"
                      << percent_util[5]/(float)hosts << ";"
                      << percent_util[6]/(float)simulator.data_center->get_num_edges() << ";" << percent_util[7]/(float)simulator.data_center->get_num_edges()
                      << ";" << percent_util[8]/(float)simulator.data_center->get_num_edges() << ";" << percent_util[9]/(float)simulator.data_center->get_num_edges()
                      << ";" << percent_util[10]/(float)simulator.data_center->get_num_edges() << ";" << percent_util[11]/(float)simulator.data_center->get_num_edges() << "\n";

      clock++;
    }

    auto end = std::chrono::steady_clock::now();

    auto diff = end - start;

    int dc_hosts = hosts;
    int dc_edges = simulator.data_center->get_num_edges();
    int dc_band = vnegpu::algorithm::sum_variable_edge(simulator.data_center, simulator.data_center->variables.edge_band);

    printf("Accepted:%u/%d.\n", accepted, simulator.num_requests);
    printf("vCPUs allocated: %u\n", vCPU_allocated);
    printf("vCPUs Clock: %u\n", vCPU_allocated_clocks);
    printf("Node Erros: %u\n", node_error);
    printf("Edge Error: %u\n", edge_error);
    printf("Total Band Asked: %u\n", total_band_asked);
    printf("Total Band used: %u\n", total_band_used);
    printf("Nodes Active per clock: %u\n", nodes_active_per_clock);
    printf("Edges Active per clock: %u\n", edges_active_per_clock);
    printf("DC Number Hosts: %u\n", dc_hosts);
    printf("DC Number Edges: %u\n", dc_edges);
    printf("DC Total Band: %u\n", dc_band);
    printf("Clocks Used: %u\n", clock);
    printf("2 Nodes:%d\n",node_2_size);
    printf("4 Nodes:%d\n",node_4_size);

    simulation_info << accepted << ";" << std::chrono::duration <double, std::milli> (diff).count() << ";" << vCPU_allocated << ";" << vCPU_allocated_clocks << ";"
    << node_error << ";" << edge_error << ";" << total_band_asked << ";" << total_band_used << ";" << nodes_active_per_clock << ";" << edges_active_per_clock
    << ";" << dc_hosts << ";" << dc_edges << ";" << dc_band << ";" << clock << ";" << node_2_size << ";" << node_4_size << "\n";
    simulation_info.close();
    fit_times.close();

    simulator.data_center->free_graph();
    simulator.base_data_center->free_graph();

    free(fragmentation_info);
    free(fingerprint_info);
    free(percent_util);
}


int main(int ac, char* av[]){
  try
  {
      int SEED;
      if(ac>1){
        SEED = atoi(av[1]);
      }else{
        SEED = 0;
      }
      char file[100];
      sprintf(file, "../simulator/Seed%d_Modelo1_WorstFit.csv", SEED);
      simulation(SEED, file, vnegpu::allocation::MODEL_1, vnegpu::allocation::worst_fit_basic_machine());
      sprintf(file, "../simulator/Seed%d_Modelo1_BestFit.csv", SEED);
      simulation(SEED, file, vnegpu::allocation::MODEL_1, vnegpu::allocation::best_fit_basic_machine());

      //simulation(1, "../simulator/Seed1_Modelo1_LRCWorstFit.csv", vnegpu::allocation::MODEL_1, vnegpu::allocation::worst_fit_basic_machine_LRC());
      //simulation(1, "../simulator/Seed1_Modelo1_LRCestFit.csv", vnegpu::allocation::MODEL_1, vnegpu::allocation::best_fit_basic_machine_LRC());

      sprintf(file, "../simulator/Seed%d_Modelo1_ZPR_WorstFit.csv", SEED);
      //simulation(SEED, file, vnegpu::allocation::MODEL_1, vnegpu::allocation::worst_fit_basic_machine_pagerank());

      sprintf(file, "../simulator/Seed%d_Modelo1_ZPR_BestFit.csv", SEED);
      //simulation(SEED, file, vnegpu::allocation::MODEL_1, vnegpu::allocation::best_fit_basic_machine_pagerank());

      //simulation(1, "../simulator/Seed1_Modelo2_WorstFit_WorstFit.csv", vnegpu::allocation::MODEL_2, vnegpu::allocation::worst_fit_basic_machine(), vnegpu::allocation::worst_fit_basic_machine_group());
      //simulation(1, "../simulator/Seed1_Modelo2_BestFit_WorstFit_CHECK.csv", vnegpu::allocation::MODEL_2, vnegpu::allocation::best_fit_basic_machine_max_check(), vnegpu::allocation::worst_fit_basic_machine_group());

      sprintf(file, "../simulator/Seed%d_Modelo2_BestFit_WorstFit.csv", SEED);
      simulation(SEED, file, vnegpu::allocation::MODEL_2, vnegpu::allocation::best_fit_basic_machine(), vnegpu::allocation::worst_fit_basic_machine_group());

      sprintf(file, "../simulator/Seed%d_Modelo2_WorstFit_BestFit.csv", SEED);
      simulation(SEED, file, vnegpu::allocation::MODEL_2, vnegpu::allocation::worst_fit_basic_machine(), vnegpu::allocation::best_fit_basic_machine_group());

      //simulation(1, "../simulator/Seed1_Modelo2_BestFit_BestFit.csv", vnegpu::allocation::MODEL_2, vnegpu::allocation::best_fit_basic_machine(), vnegpu::allocation::best_fit_basic_machine_group());

      sprintf(file, "../simulator/Seed%d_Modelo3_WorstFit.csv", SEED);
      simulation(SEED, file, vnegpu::allocation::MODEL_3, vnegpu::allocation::worst_fit_basic_machine());

      sprintf(file, "../simulator/Seed%d_Modelo3_BestFit.csv", SEED);
      simulation(SEED, file, vnegpu::allocation::MODEL_3, vnegpu::allocation::best_fit_basic_machine());

      //simulation(1, "../simulator/Seed1_Modelo4_WorstFit_WorstFit.csv", vnegpu::allocation::MODEL_4, vnegpu::allocation::worst_fit_basic_machine(), vnegpu::allocation::worst_fit_basic_machine_group());
      //simulation(1, "../simulator/Seed1_Modelo4_BestFit_WorstFit_CHECK.csv", vnegpu::allocation::MODEL_4, vnegpu::allocation::best_fit_basic_machine_max_check(), vnegpu::allocation::worst_fit_basic_machine_group());

      sprintf(file, "../simulator/Seed%d_Modelo4_BestFit_WorstFit.csv", SEED);
      simulation(SEED, file, vnegpu::allocation::MODEL_4, vnegpu::allocation::best_fit_basic_machine(), vnegpu::allocation::worst_fit_basic_machine_group());

      sprintf(file, "../simulator/Seed%d_Modelo4_WorstFit_BestFit.csv", SEED);
      simulation(SEED, file, vnegpu::allocation::MODEL_4, vnegpu::allocation::worst_fit_basic_machine(), vnegpu::allocation::best_fit_basic_machine_group());
      //simulation(1, "../simulator/Seed1_Modelo4_BestFit_BestFit.csv", vnegpu::allocation::MODEL_4, vnegpu::allocation::best_fit_basic_machine(), vnegpu::allocation::best_fit_basic_machine_group());

      cudaDeviceReset();

  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }
}

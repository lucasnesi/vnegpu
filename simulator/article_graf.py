#!/usr/bin/python3
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys
import matplotlib
import csv
from pathlib import Path
from matplotlib.ticker import FuncFormatter
import os
import glob
import math
import matplotlib.patches as mpatches
from matplotlib.spines import Spine
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection

font = {'family' : 'Arial',
        'size'   : 14}

matplotlib.rc('font', **font)

#modes = ["Modelo1_BestFit", "Modelo1_WorstFit", "Modelo2_BestFit_WorstFit", "Modelo2_WorstFit_BestFit", "Modelo3_BestFit", "Modelo3_WorstFit", "Modelo4_BestFit_WorstFit", "Modelo4_WorstFit_BestFit"]
#names = ["1-BF", "1-WF", "2-BFWF", "2-WFBF", "3-BF", "3-WF", "4-BFWF", "4-WFBF"]

modes = ["Modelo1_BestFit", "Modelo1_WorstFit", "Modelo2_WorstFit_BestFit", "Modelo4_WorstFit_BestFit"]
names = ["1-BF", "1-WF", "2-WFBF", "4-WFBF"]


num_linhas = 2
num_colunas = 2
num_files = 20
nomesave = "x_"


class Radar(object):

    def __init__(self, fig, titles, labels, rect=None):
        if rect is None:
            rect = [0.05, 0.08, 0.85, 0.85]

        self.n = len(titles)
        self.angles = np.arange(90, 90+360, 360.0/self.n)
        self.angles = [a % 360 for a in self.angles]
        self.axes = [fig.add_axes(rect, projection="polar", label="axes%d" % i)
                         for i in range(self.n)]

        self.ax = self.axes[0]
        self.ax.grid(linewidth=1)
        self.ax.set_thetagrids(self.angles, labels=titles, fontsize=16)

        for ax in self.axes[1:]:
            ax.patch.set_visible(False)
            ax.grid("off")
            ax.xaxis.set_visible(False)

        for ax, angle, label in zip(self.axes, self.angles, labels):
            ax.set_rgrids(range(1, 6), angle=0, labels=[""*len(titles)], fontsize=6, family='sans-serif', weight='light')
            ax.spines["polar"].set_visible(False)
            ax.set_ylim(0, 5)

        #self.ax.set_rgrids(range(1, 6), angle=22, labels=labels[0], fontsize=8, family='sans-serif', weight='normal')
        #self.axes[0].spines["polar"].set_visible(True)

    def plot(self, values, *args, **kw):
        angle = np.deg2rad(np.r_[self.angles, self.angles[0]])
        values = np.r_[values, values[0]]
        self.ax.plot(angle, values, *args, **kw)

    def fill(self, values, *args, **kw):
        angle = np.deg2rad(np.r_[self.angles, self.angles[0]])
        values = np.r_[values, values[0]]
        self.ax.fill(angle, values, *args, **kw)


def pre(f):
    return "%0.2f" % f

def pre_u(f):
    return "%0.2f" % (f/1000)

def ler(file):
    fragmentacao_hosts = np.asarray([])
    fragmentacao_enlaces = np.asarray([])

    fingerprint_hosts_cpu = np.asarray([])
    fingerprint_hosts_mem = np.asarray([])
    fingerprint_switchs = np.asarray([])

    percent_host_0 = np.asarray([])
    percent_host_20 = np.asarray([])
    percent_host_40 = np.asarray([])
    percent_host_60 = np.asarray([])
    percent_host_80 = np.asarray([])
    percent_host_100 = np.asarray([])

    percent_link_0 = np.asarray([])
    percent_link_20 = np.asarray([])
    percent_link_40 = np.asarray([])
    percent_link_60 = np.asarray([])
    percent_link_80 = np.asarray([])
    percent_link_100 = np.asarray([])

    with open(file, 'r') as csvfile:
        spamreader = list(csv.reader(csvfile, delimiter=';'))
        if(len(spamreader)==0):
            return
        for row in spamreader[:100]:
            fragmentacao_hosts = np.append(fragmentacao_hosts, float(row[0]))
            fragmentacao_enlaces = np.append(fragmentacao_enlaces, float(row[1]))
            fingerprint_hosts_cpu = np.append(fingerprint_hosts_cpu, float(row[2]))
            fingerprint_hosts_mem = np.append(fingerprint_hosts_mem, float(row[3]))
            fingerprint_switchs = np.append(fingerprint_switchs, float(row[4]))
            if(len(row)>5):
                percent_host_0 = np.append(percent_host_0, float(row[5]))
                percent_host_20 = np.append(percent_host_20, float(row[6]))
                percent_host_40 = np.append(percent_host_40, float(row[7]))
                percent_host_60 = np.append(percent_host_60, float(row[8]))
                percent_host_80 = np.append(percent_host_80, float(row[9]))
                percent_host_100 = np.append(percent_host_100, float(row[10]))
                percent_link_0 = np.append(percent_link_0, float(row[11]))
                percent_link_20 = np.append(percent_link_20, float(row[12]))
                percent_link_40 = np.append(percent_link_40, float(row[13]))
                percent_link_60 = np.append(percent_link_60, float(row[14]))
                percent_link_80 = np.append(percent_link_80, float(row[15]))
                percent_link_100 = np.append(percent_link_100, float(row[16]))

        accepted = int(spamreader[-1][0])
        time = float(spamreader[-1][1])
        vcpu = float(spamreader[-1][2])
        vcpu_clock = float(spamreader[-1][3])
        node_fail = float(spamreader[-1][4])
        edge_fail = float(spamreader[-1][5])
        lucro=0
        custo=0
        nodes_active_per_clock = 0
        edges_active_per_clock = 0
        dc_hosts = 0
        dc_edges = 0
        dc_band = 0
        clock = 0
        node_2_size = 0
        node_4_size = 0
        if(len(spamreader[-1])>6):
            lucro = float(spamreader[-1][6])
            custo = float(spamreader[-1][7])
        if(len(spamreader[-1])>8):
            nodes_active_per_clock = float(spamreader[-1][8])
            edges_active_per_clock = float(spamreader[-1][9])
            dc_hosts = float(spamreader[-1][10])
            dc_edges = float(spamreader[-1][11])
            dc_band = float(spamreader[-1][12])
            clock = float(spamreader[-1][13])
            node_2_size = float(spamreader[-1][14])
            node_4_size = float(spamreader[-1][15])

    return fragmentacao_hosts, fragmentacao_enlaces, fingerprint_hosts_cpu, fingerprint_hosts_mem, fingerprint_switchs,\
    percent_host_0, percent_host_20, percent_host_40, percent_host_60, percent_host_80, percent_host_100, percent_link_0, percent_link_20, percent_link_40,\
    percent_link_60, percent_link_80, percent_link_100, accepted, time, vcpu, vcpu_clock, node_fail, edge_fail, lucro, custo, nodes_active_per_clock, edges_active_per_clock,\
    dc_hosts, dc_edges, dc_band, clock, node_2_size, node_4_size


def ler_times(file):
    fragmentacao_hosts = np.asarray([])
    fragmentacao_enlaces = np.asarray([])


    with open(file, 'r') as csvfile:
        spamreader = list(csv.reader(csvfile, delimiter=';'))
        if(len(spamreader)==0):
            return
        for row in spamreader:
            fragmentacao_hosts = np.append(fragmentacao_hosts, float(row[0]))
            fragmentacao_enlaces = np.append(fragmentacao_enlaces, float(row[1]))

    return fragmentacao_hosts, fragmentacao_enlaces


def f_numpy(names, values):
    result_names = np.unique(names)
    result_values = np.empty(result_names.shape)

    for i, name in enumerate(result_names):
        result_values[i] = np.mean(values[names == name])

    return result_names, result_values

cores = ['m', 'g', 'y', 'c', 'r', 'b', "orange", 'k']

cores_8 = ['m', 'g', 'r', 'y', 'k', 'b', "orange", 'c']

def max10(x):
    return math.ceil( (np.amax(x)*1.1) / 1000.0) * 1000.0


def millions(x, pos):
    return '%1.1fM' % (x*1e-6)


def fast_graf_combo_bar_ax(ax, nome, x_name, y_name, data, scale, lim_i, lim_u, form=None):
    error_config = {'ecolor': '0.3'}
    for i in range(len(modes)):
        print(np.mean(data[i]))
        ax.bar(i, np.mean(data[i])/scale, 0.9,
                 alpha=0.6,
                 color=cores_8[i],
                 yerr=np.std(data[i])/scale,
                 error_kw=error_config,
                 label=names[i])

    #ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid()

    if(y_name!=None):
        ax.set_ylabel(y_name, fontsize=16)
    else:
        ax.set_yticklabels([])
        ax.yaxis.set_ticks_position('none')

    if(x_name!=None):
        ax.set_xlabel(x_name, fontsize=16)

    if(nome!=None):
        ax.set_title(nome, fontsize=18)

    ax.set_xticks([])
    #ax.set_xticklabels(names)

    if form is not None and y_name!=None:
        formatter = FuncFormatter(form)
        ax.yaxis.set_major_formatter(formatter)
    #ax.set_title('Acceptance ratio')
    ax.set_ylim([lim_i,lim_u])
    #ax.set_xlim([-0.0001,1.01])
    #ax.set_xticks([0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
    ax.set_yticks(np.arange(0, (lim_u/5)*6, lim_u/5))


marks = ['o', '<', 'x', '.']
ss = [8, 8, 6, 4, 4, 4, 4, 4]

def dot(x, pos):
    if x==0 or x==1:
        return '%1.0f' % (x)
    return '%1.1f' % (x)

#size-(i*0.8)

def f_numpy(names, values):
    result_names = np.unique(names)
    result_values = np.empty(result_names.shape)

    for i, name in enumerate(result_names):
        result_values[i] = np.mean(values[names == name])

    return result_names, result_values

sm = [4, 4, 4, 4, 4, 4, 4, 4]

def fast_graf_combo_dots_ax_time(ax, nome, x_name, y_name, data_x, data_y, lim_i=None, lim_u=None, size=1, line="None"):
    opacity = 0.6

    for i in range(len(modes)):
        a_x = np.array(data_x[i])
        a_y = np.array(data_y[i])

        fd = a_x[(a_y >= 0) & (a_y <= 300)]
        fe = a_y[(a_y >= 0) & (a_y <= 300)]
        af2, af1 = f_numpy(fd, fe)
        ax.plot(af2,af1,color=cores[i], linestyle="None", marker='.',label=names[i],markersize=sm[i], linewidth=1, alpha=0.5)

    #ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid()
    if(y_name!=None):
        ax.set_ylabel(y_name, fontsize=14)
    else:
        ax.set_yticklabels([])
        ax.yaxis.set_ticks_position('none')

    if(x_name!=None):
        ax.set_xlabel(x_name, fontsize=14)

    if(nome!=None):
        ax.set_title(nome, fontsize=16)

    ax.set_ylim([49,251])
    ax.set_xlim([79,401])
    ax.set_yticks([50, 100, 150, 200, 250])
    ax.set_xticks([100, 200, 300, 400])

    if(lim_i!=None):
        ax.set_ylim([lim_i,lim_u])
        ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1])
        formatter = FuncFormatter(dot)
        ax.xaxis.set_major_formatter(formatter)
    #ax.set_xlim([-0.01,1.01])
    #
def fast_graf_combo_dots_ax(ax, nome, x_name, y_name, data_x, data_y, lim_i=None, lim_u=None, size=1, line="None"):
    opacity = 0.6

    for i in range(len(modes)):
        ax.plot(data_x[i],data_y[i],color=cores[i], linestyle="None", marker='.',label=names[i],markersize=ss[i], linewidth=1, alpha=0.5)

    #ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid()
    if(y_name!=None):
        ax.set_ylabel(y_name, fontsize=16)
    else:
        ax.set_yticklabels([])
        ax.yaxis.set_ticks_position('none')

    if(x_name!=None):
        ax.set_xlabel(x_name, fontsize=16)

    if(nome!=None):
        ax.set_title(nome, fontsize=18)

    if(lim_i!=None):
        ax.set_ylim([lim_i,lim_u])
        ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1])
        formatter = FuncFormatter(dot)
        ax.xaxis.set_major_formatter(formatter)
    #ax.set_xlim([-0.01,1.01])



    if(lim_u!=None):
        ax.set_yticks(np.arange(0, (lim_u/5)*6, lim_u/5))

def fast_graf_line_ax_util(ax, nome, x_name, y_name, data_x, data_y, folder, mode, lim_i=None, lim_u=None, size=1):
    opacity = 0.6

    for i in range(6):
        mea = np.mean(data_y[i][folder][mode], axis=0)
        std = np.std(data_y[i][folder][mode], axis=0)
        ax.plot(data_x, mea, color=cores[i], linestyle='-', label='', linewidth=size, alpha=opacity)
        ax.fill_between(data_x, mea-std, mea+std, facecolor=cores[i], alpha=0.25, edgecolor='none')

    #ax.set_xticks(np.arange(0, len(modes), 1))
    ax.yaxis.grid()
    if(y_name!=None):
        ax.set_ylabel(y_name, fontsize=14)
    else:
        ax.set_yticklabels([])
        ax.yaxis.set_ticks_position('none')

    if(x_name!=None):
        ax.set_xlabel(x_name, fontsize=14)

    if(nome!=None):
        ax.set_title(nome, fontsize=16, y=1.03)

    #if(lim_i!=None and lim_u!=None):
    ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1])
    ax.set_xticks([0, 20, 40, 60, 80, 100])
    ax.set_ylim([-0.0001,1.0001])
    ax.set_xlim([-0.1,100.01])


def radar_norm(data, fl, i):
    means = []
    for y in data:
        means.append(np.mean(y, axis=1))
    if(means[fl][i]==0):
        return 0
    return means[fl][i]/np.amax(means[fl])*5

folders = ["fat_req_01/", "bcube_kmeans_30/" , "dcell_70/"]
folder_name = ["Fat Tree $40$", "Bcube $7, 4$", "DCell $11, 2$"]

_FFT = 0
_FBC = 1
_FDC = 2

def main(argv):



    global nomesave
    nomesave = "article_"

    num_folders = len(folders)

    path = folders[_FFT]+"Seed*" + modes[0] + ".csv"

    global num_files
    num_files = len(sorted(glob.glob(path)))

    print(num_files)

    num_modos = len(modes)

    fragmentacao_hosts = np.zeros((num_folders, num_modos, num_files, 100))
    fragmentacao_enlaces = np.zeros((num_folders, num_modos, num_files, 100))
    finger_cpu = np.zeros((num_folders, num_modos, num_files, 100))
    finger_mem = np.zeros((num_folders, num_modos, num_files, 100))
    finger_band = np.zeros((num_folders, num_modos, num_files, 100))
    util_hosts_0 = np.zeros((num_folders, num_modos, num_files, 100))
    util_hosts_20 = np.zeros((num_folders, num_modos, num_files, 100))
    util_hosts_40 = np.zeros((num_folders, num_modos, num_files, 100))
    util_hosts_60 = np.zeros((num_folders, num_modos, num_files, 100))
    util_hosts_80 = np.zeros((num_folders, num_modos, num_files, 100))
    util_hosts_100 = np.zeros((num_folders, num_modos, num_files, 100))
    util_enlaces_0 = np.zeros((num_folders, num_modos, num_files, 100))
    util_enlaces_20 = np.zeros((num_folders, num_modos, num_files, 100))
    util_enlaces_40 = np.zeros((num_folders, num_modos, num_files, 100))
    util_enlaces_60 = np.zeros((num_folders, num_modos, num_files, 100))
    util_enlaces_80 = np.zeros((num_folders, num_modos, num_files, 100))
    util_enlaces_100 = np.zeros((num_folders, num_modos, num_files, 100))

    aloc_tempos = []
    aloc_size = []

    for y in range(num_folders):
        aloc_tempos.append([])
        aloc_size.append([])
        for i in range(num_modos):
            aloc_tempos[y].append([])
            aloc_size[y].append([])

    aceitos = np.zeros((num_folders, num_modos, num_files))
    tempo = np.zeros((num_folders, num_modos, num_files))
    vcpus = np.zeros((num_folders, num_modos, num_files))
    vcpus_clock = np.zeros((num_folders, num_modos, num_files))
    node_fail = np.zeros((num_folders, num_modos, num_files))
    edge_fail = np.zeros((num_folders, num_modos, num_files))
    lucro = np.zeros((num_folders, num_modos, num_files))
    custo = np.zeros((num_folders, num_modos, num_files))
    nodes_active_per_clock = np.zeros((num_folders, num_modos, num_files))
    edges_active_per_clock = np.zeros((num_folders, num_modos, num_files))
    dc_hosts = np.zeros((num_folders, num_modos, num_files))
    dc_edges = np.zeros((num_folders, num_modos, num_files))
    dc_band = np.zeros((num_folders, num_modos, num_files))
    clock = np.zeros((num_folders, num_modos, num_files))
    node_2_size = np.zeros((num_folders, num_modos, num_files))
    node_4_size = np.zeros((num_folders, num_modos, num_files))

    fol_atual = 0
    for fol in folders:
        modo_num = 0
        print(fol_atual)
        for mod in modes:

            path = fol + "*" + mod + ".csv"
            print(path)
            seed = 0
            for file in sorted(glob.glob(path)):
                print(file)
                fh, fe, fc, fm, fs, ph0, ph2, ph4, ph6, ph8, ph10, pl0, pl2, pl4, pl6, pl8, pl10, ac, ti, vc, vcc, nf, ef, lu, cu, nc, ne, dh, de, db, c, n2, n4 = ler(file)
                fragmentacao_hosts[fol_atual][modo_num][seed] = fh
                fragmentacao_enlaces[fol_atual][modo_num][seed] = fe
                finger_cpu[fol_atual][modo_num][seed] = fc
                finger_mem[fol_atual][modo_num][seed] = fm
                finger_band[fol_atual][modo_num][seed] = fs
                util_hosts_0[fol_atual][modo_num][seed] = ph0
                util_hosts_20[fol_atual][modo_num][seed] = ph2
                util_hosts_40[fol_atual][modo_num][seed] = ph4
                util_hosts_60[fol_atual][modo_num][seed] = ph6
                util_hosts_80[fol_atual][modo_num][seed] = ph8
                util_hosts_100[fol_atual][modo_num][seed] = ph10
                util_enlaces_0[fol_atual][modo_num][seed] = pl0
                util_enlaces_20[fol_atual][modo_num][seed] = pl2
                util_enlaces_40[fol_atual][modo_num][seed] = pl4
                util_enlaces_60[fol_atual][modo_num][seed] = pl6
                util_enlaces_80[fol_atual][modo_num][seed] = pl8
                util_enlaces_100[fol_atual][modo_num][seed] = pl10
                aceitos[fol_atual][modo_num][seed] = ac
                tempo[fol_atual][modo_num][seed] = ti
                vcpus[fol_atual][modo_num][seed] = vc
                vcpus_clock[fol_atual][modo_num][seed] = vcc
                node_fail[fol_atual][modo_num][seed] = nf
                edge_fail[fol_atual][modo_num][seed] = ef
                lucro[fol_atual][modo_num][seed] = lu
                custo[fol_atual][modo_num][seed] = cu
                nodes_active_per_clock[fol_atual][modo_num][seed] = nc
                edges_active_per_clock[fol_atual][modo_num][seed] = ne
                dc_hosts[fol_atual][modo_num][seed] = dh
                dc_edges[fol_atual][modo_num][seed] = de
                dc_band[fol_atual][modo_num][seed] = db
                clock[fol_atual][modo_num][seed] = c
                node_2_size[fol_atual][modo_num][seed] = n2
                node_4_size[fol_atual][modo_num][seed] = n4
                seed+=1
                if(seed==num_files):
                    break

            modo_num+=1

        modo_num = 0
        for mod in modes:

            path = fol+"*" + mod + ".csv_times"
            seed = 0
            for file in sorted(glob.glob(path)):
                print(file)
                at, ass = ler_times(file)

                aloc_tempos[fol_atual][modo_num].extend(at)
                aloc_size[fol_atual][modo_num].extend(ass)

                seed+=1
                if(seed==num_files):
                    break
            modo_num+=1

        fol_atual+=1

    ############################
    #####    GRAFICOS   ########
    ###########################

    f, axarr = plt.subplots(3, 2)
    f.set_figheight(9)
    f.set_figwidth(9)
    fast_graf_combo_dots_ax(axarr[0, 0], "Fat-Tree 40", "Fragmentation", "DC Servers Footprint", fragmentacao_hosts[_FFT], finger_cpu[_FFT], size=10, lim_i=-0.0001, lim_u=1.0001)
    fast_graf_combo_dots_ax(axarr[0, 1], "Fat-Tree 40", "Fragmentation", "DC Links Footprint", fragmentacao_enlaces[_FFT], finger_band[_FFT], size=10, lim_i=-0.0001, lim_u=0.1)

    fast_graf_combo_dots_ax(axarr[1, 0], "Bcube 7, 4", "Fragmentation", "DC Servers Footprint", fragmentacao_hosts[_FBC], finger_cpu[_FBC], size=10, lim_i=-0.0001, lim_u=1.0001)
    fast_graf_combo_dots_ax(axarr[1, 1], "Bcube 7, 4", "Fragmentation", "DC Links Footprint", fragmentacao_enlaces[_FBC], finger_band[_FBC], size=10, lim_i=-0.0001, lim_u=0.3)

    fast_graf_combo_dots_ax(axarr[2, 0], "DCell 11, 2", "Fragmentation", "DC Servers Footprint", fragmentacao_hosts[_FDC], finger_cpu[_FDC], size=10, lim_i=-0.0001, lim_u=1.0001)
    fast_graf_combo_dots_ax(axarr[2, 1], "DCell 11, 2", "Fragmentation", "DC Links Footprint", fragmentacao_enlaces[_FDC], finger_band[_FDC], size=10, lim_i=-0.0001, lim_u=0.7)

    #trans = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 0].transAxes)
    #line = plt.Line2D([0.381, 0.381], [-1.905, 1.2], color='#d6d6d6', alpha=0.8, linewidth=1, transform=trans)
    #f.lines.append(line)
    #trans2 = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 1].transAxes)
    #line2 = plt.Line2D([0.6875, 0.6875], [-1.9, 1.2], color='#d6d6d6', alpha=0.8, linewidth=1, transform=trans2)
    #f.lines.append(line2)

    opacity = 0.6
    legen = []
    for i in range(len(modes)):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=cores[i], alpha=opacity, label=''))
    plt.tight_layout(pad=0.4, w_pad=0.2, h_pad=0.4)
    lgd = plt.legend(handles=legen, labels=names, loc='lower center', ncol=4, fontsize=16, bbox_to_anchor=(-0.1, -0.55))
    f.savefig(nomesave + "foot_frag.pdf", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()




    ############################
    #####   Utilizacao  ########
    ###########################

    clock_values = np.arange(0, 100, 1)

    info = [util_hosts_0, util_hosts_20, util_hosts_40, util_hosts_60, util_hosts_80, util_hosts_100]

    f, axarr = plt.subplots(2, 6)
    f.set_figheight(5)
    f.set_figwidth(18)
    siz = 2
    fast_graf_line_ax_util(axarr[0, 0], "(a) " + names[0], "Interval", "DC Utilization", clock_values, info, _FFT, 0, size=siz)
    fast_graf_line_ax_util(axarr[0, 1], "(b) " + names[1], "Interval", None, clock_values, info, _FFT, 1, size=siz)
    fast_graf_line_ax_util(axarr[0, 2], "(e) " + names[0], "Interval", None, clock_values, info, _FBC, 0, size=siz)
    fast_graf_line_ax_util(axarr[0, 3], "(f) " + names[1], "Interval", None, clock_values, info, _FBC, 1, size=siz)
    fast_graf_line_ax_util(axarr[0, 4], "(i) " + names[0], "Interval", None, clock_values, info, _FDC, 0, size=siz)
    fast_graf_line_ax_util(axarr[0, 5], "(j) " + names[1], "Interval", None, clock_values, info, _FDC, 1, size=siz)

    fast_graf_line_ax_util(axarr[1, 0], "(c) " + names[2], "Interval", "DC Utilization", clock_values, info, _FFT, 2, size=siz)
    fast_graf_line_ax_util(axarr[1, 1], "(d) " + names[3], "Interval", None, clock_values, info, _FFT, 3, size=siz)
    fast_graf_line_ax_util(axarr[1, 2], "(g) " + names[2], "Interval", None, clock_values, info, _FBC, 2, size=siz)
    fast_graf_line_ax_util(axarr[1, 3], "(h) " + names[3], "Interval", None, clock_values, info, _FBC, 3, size=siz)
    fast_graf_line_ax_util(axarr[1, 4], "(k) " + names[2], "Interval", None, clock_values, info, _FDC, 2, size=siz)
    fast_graf_line_ax_util(axarr[1, 5], "(l) " + names[3], "Interval", None, clock_values, info, _FDC, 3, size=siz)


    #axarr[0, 0].set_title("Fat-Tree 40", fontsize=16, y=1.1, x=1.7, loc='right')
    #axarr[0, 2].set_title("BCube 7, 4", fontsize=16, y=1.1, x=1.7, loc='right')
    #axarr[0, 4].set_title("DCell X, Y", fontsize=16, y=1.1, x=1.7, loc='right')

    #t1 = plt.text(5.0, 2.8, "Dcell", fontsize=16)

    trans = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 0].transAxes)
    line = plt.Line2D([0.350, 0.350], [-1.5, 1.2], color='k', alpha=0.3, linewidth=1, transform=trans)
    f.lines.append(line)
    trans2 = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 1].transAxes)
    line2 = plt.Line2D([0.674, 0.674], [-1.5, 1.2], color='k', alpha=0.3, linewidth=1, transform=trans2)
    f.lines.append(line2)

    opacity = 0.6
    hand = []
    for i in range(6):
        hand.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='_', markersize=15, label=''))
    plt.tight_layout(pad=0.4, w_pad=0.2, h_pad=0.4)
    lgd = plt.legend(handles=hand, labels=["0%", "1%-20%", "21%-40%", "41%-60%", "61%-80%", "81%-100%"], loc='lower center', ncol=6, fontsize=18, bbox_to_anchor=(-2.2, -0.6))
    f.savefig(nomesave + "util_hosts.pdf", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()





    info = [util_enlaces_0, util_enlaces_20, util_enlaces_40, util_enlaces_60, util_enlaces_80, util_enlaces_100]

    f, axarr = plt.subplots(2, 6)
    f.set_figheight(5)
    f.set_figwidth(18)
    siz = 2
    fast_graf_line_ax_util(axarr[0, 0], "(a) " + names[0], "Interval", "Links Utilization", clock_values, info, _FFT, 0, size=siz)
    fast_graf_line_ax_util(axarr[0, 1], "(b) " + names[1], "Interval", None, clock_values, info, _FFT, 1, size=siz)
    fast_graf_line_ax_util(axarr[0, 2], "(e) " + names[0], "Interval", None, clock_values, info, _FBC, 0, size=siz)
    fast_graf_line_ax_util(axarr[0, 3], "(f) " + names[1], "Interval", None, clock_values, info, _FBC, 1, size=siz)
    fast_graf_line_ax_util(axarr[0, 4], "(i) " + names[0], "Interval", None, clock_values, info, _FDC, 0, size=siz)
    fast_graf_line_ax_util(axarr[0, 5], "(j) " + names[1], "Interval", None, clock_values, info, _FDC, 1, size=siz)

    fast_graf_line_ax_util(axarr[1, 0], "(c) " + names[2], "Interval", "Links Utilization", clock_values, info, _FFT, 2, size=siz)
    fast_graf_line_ax_util(axarr[1, 1], "(d) " + names[3], "Interval", None, clock_values, info, _FFT, 3, size=siz)
    fast_graf_line_ax_util(axarr[1, 2], "(g) " + names[2], "Interval", None, clock_values, info, _FBC, 2, size=siz)
    fast_graf_line_ax_util(axarr[1, 3], "(h) " + names[3], "Interval", None, clock_values, info, _FBC, 3, size=siz)
    fast_graf_line_ax_util(axarr[1, 4], "(k) " + names[2], "Interval", None, clock_values, info, _FDC, 2, size=siz)
    fast_graf_line_ax_util(axarr[1, 5], "(l) " + names[3], "Interval", None, clock_values, info, _FDC, 3, size=siz)

    #axarr[0, 0].set_title("Fat-Tree 40", fontsize=16, y=1.1, x=1.7, loc='right')
    #axarr[0, 2].set_title("BCube 7, 4", fontsize=16, y=1.1, x=1.7, loc='right')
    #axarr[0, 4].set_title("DCell X, Y", fontsize=16, y=1.1, x=1.7, loc='right')

    #t1 = plt.text(5.0, 2.8, "Dcell", fontsize=16)

    #trans = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 0].transAxes)
    #line = plt.Line2D([0.350, 0.350], [-1.5, 1.2], color='k', alpha=0.3, linewidth=1, transform=trans)
    #f.lines.append(line)
    #trans2 = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 1].transAxes)
    #line2 = plt.Line2D([0.674, 0.674], [-1.5, 1.2], color='k', alpha=0.3, linewidth=1, transform=trans2)
    #f.lines.append(line2)

    opacity = 0.6
    hand = []
    for i in range(6):
        hand.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='_', markersize=15, label=''))
    plt.tight_layout(pad=0.4, w_pad=0.2, h_pad=0.4)
    lgd = plt.legend(handles=hand, labels=["0%", "1%-20%", "21%-40%", "41%-60%", "61%-80%", "81%-100%"], loc='lower center', ncol=6, fontsize=18, bbox_to_anchor=(-2.2, -0.7))
    f.savefig(nomesave + "util_enlaces.svg", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()

    ############################
    #####     Barras   ########
    ###########################

    f, axarr = plt.subplots(2, 3)
    f.set_figheight(4)
    f.set_figwidth(9)
    fast_graf_combo_bar_ax(axarr[0, 0], "Fat-Tree 40", "Strategy", "Acceptance Rate", aceitos[_FFT], 3000, -0.0001, 1.0001)
    fast_graf_combo_bar_ax(axarr[0, 1], "Bcube 7, 4", "Strategy", None, aceitos[_FBC], 3000, -0.0001, 1.0001)
    fast_graf_combo_bar_ax(axarr[0, 2], "DCell 11, 2", "Strategy", None, aceitos[_FDC], 3000, -0.0001, 1.0001)

    print("a")
    print(np.mean(vcpus[_FFT], axis=1))

    fast_graf_combo_bar_ax(axarr[1, 0], None, "Strategy", "vCPUs Allocated", vcpus[_FFT], 1, np.amin(vcpus)*0.98, np.amax(vcpus)*1.01, millions)
    fast_graf_combo_bar_ax(axarr[1, 1], None, "Strategy", None, vcpus[_FBC], 1, np.amin(vcpus)*0.98, np.amax(vcpus)*1.01, millions)
    fast_graf_combo_bar_ax(axarr[1, 2], None, "Strategy", None, vcpus[_FDC], 1, np.amin(vcpus)*0.98, np.amax(vcpus)*1.01, millions)

    trans = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 0].transAxes)
    line = plt.Line2D([0.391, 0.391], [-1.455, 1.2], color='#d6d6d6', alpha=0.8, linewidth=1, transform=trans)
    f.lines.append(line)
    trans2 = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 1].transAxes)
    line2 = plt.Line2D([0.6923, 0.6923], [-1.46, 1.2], color='#d6d6d6', alpha=0.8, linewidth=1, transform=trans2)
    f.lines.append(line2)

    opacity = 0.6
    legen = []
    for i in range(len(modes)):
        #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
        legen.append(mpatches.Patch(color=cores_8[i], alpha=opacity, label=''))
    plt.tight_layout(pad=0.4, w_pad=0.2, h_pad=0.2)
    lgd = plt.legend(handles=legen, labels=names, loc='lower center', ncol=4, fontsize=16, bbox_to_anchor=(-0.55, -0.78))
    f.savefig(nomesave + "bars.pdf", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
    plt.close()


    ############################
    #####     RADAR    ########
    ###########################

    for fl in range(len(folders)):
        fig = plt.figure(figsize=(3, 3))

        titles = ["AR","NR","ER","C/L", "2S", "4S", "FN", "FE"]

        labels = [
            [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0], [0.2,0.4,0.6,0.8,1.0]
        ]

        radar = Radar(fig, titles, labels)
        c_l = custo/lucro
        c_l[np.isnan(c_l)] = 0

        pre_data = []

        for i in range(len(modes)):
            pre_data = [radar_norm(aceitos, fl, i), radar_norm(node_fail, fl, i), radar_norm(edge_fail, fl, i), radar_norm(c_l, fl, i),  radar_norm(node_2_size, fl, i),  radar_norm(node_4_size, fl, i),  radar_norm(nodes_active_per_clock, fl, i),  radar_norm(edges_active_per_clock, fl, i)];
            radar.plot(pre_data,  "-", lw=2, color=cores[i], alpha=0.7, label=names[i])
            radar.fill(pre_data,  "-", lw=2, color=cores[i], alpha=0.05)
            #radar.plot([np.mean(aceitos[i])/3000*5, np.mean(node_fail[i])/3000*10, np.mean(edge_fail[i])/3000*10,  np.mean(custo[i])/ np.mean(lucro[i])/2],  "-", lw=2, color=cores[i], alpha=0.7, label=names[i])

        #radar.ax.legend()

        opacity = 0.6
        legen = []
        for i in range(len(modes)):
            #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
            legen.append(mpatches.Patch(color=cores[i], alpha=opacity, label=''))


        if(fl==1):
            lgd = plt.legend(handles=legen, labels=names, loc='lower center', ncol=len(modes), fontsize=16, bbox_to_anchor=(0.5, -0.3))
            fig.savefig(nomesave + "radar_" + str(fl) + ".pdf", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight', transparent=True)
        else:
            fig.savefig(nomesave + "radar_" + str(fl) + ".pdf", dpi=200, bbox_inches='tight', transparent=True)
        plt.close()


    ############################
    #####     TABELA   ########
    ###########################

    if(len(modes)==4):
        print("\\begin{table}[!ht]")
        print("\centering")
        print("\scriptsize")
        print("\caption{\\ac{VI} allocation execution time. TODO: colocar os tempos.}")
        print("\label{tab:time}")
        print("\\begin{tabular}{|c|c|c|c|c|}")
        print("\hline")
        print("\\textbf{Model}  & \\textbf{Topology} & \\textbf{Average}  & \\textbf{Min.} & \\textbf{Max.} \\\\")
        print("\hline")


        table_names = ["$1$-\\ac{BF}", "$1$-\\ac{WF}", "$2$-\\ac{WF}\\ac{BF}", "$4$-\\ac{WF}\\ac{BF}"]


        for i in range(len(modes)):
            for y in range(len(folders)):
                if(y==0):
                    print("\multirow{3}{*}{"+ table_names[i] +"}  & " + folder_name[y] + " & $" + pre(np.percentile(aloc_tempos[y][i], 25)) + "$ & $" +\
                    pre(np.percentile(aloc_tempos[y][i], 50)) + "$  & $" + pre(np.percentile(aloc_tempos[y][i], 99)) + "$ \\\\ ")
                else:
                    print(" & " + folder_name[y] + " & $" + pre(np.percentile(aloc_tempos[y][i], 25)) + "$ & $" +\
                    pre(np.percentile(aloc_tempos[y][i], 50)) + "$  & $" + pre(np.percentile(aloc_tempos[y][i], 99)) + "$ \\\\ ")
                if(y<2):
                    print("\hhline{~----}")
                else:
                    print("\\thickhline")

        print("\end{tabular}")
        print("\end{table}")

        ############################
        #####    tempos   ########
        ###########################

        f, axarr = plt.subplots(1, 3)
        f.set_figheight(2)
        f.set_figwidth(8)

        fast_graf_combo_dots_ax_time(axarr[0], "Fat-Tree 40", "VI size", "Time (ms)", aloc_size[_FFT], aloc_tempos[_FFT], size=10)
        fast_graf_combo_dots_ax_time(axarr[1], "BCube 7, 4", "VI size", None, aloc_size[_FBC], aloc_tempos[_FBC], size=10)
        fast_graf_combo_dots_ax_time(axarr[2], "DCell 11, 2", "VI size", None, aloc_size[_FDC], aloc_tempos[_FDC], size=10)


        #trans = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 0].transAxes)
        #line = plt.Line2D([0.381, 0.381], [-1.905, 1.2], color='#d6d6d6', alpha=0.8, linewidth=1, transform=trans)
        #f.lines.append(line)
        #trans2 = matplotlib.transforms.blended_transform_factory(f.transFigure, axarr[0, 1].transAxes)
        #line2 = plt.Line2D([0.6875, 0.6875], [-1.9, 1.2], color='#d6d6d6', alpha=0.8, linewidth=1, transform=trans2)
        #f.lines.append(line2)

        opacity = 0.6
        legen = []
        for i in range(len(modes)):
            #legen.append(plt.Line2D([], [], color=cores[i], alpha=opacity, marker='s', markersize=15, label=''))
            legen.append(mpatches.Patch(color=cores[i], alpha=opacity, label=''))
        plt.tight_layout(pad=0.4, w_pad=0.2, h_pad=0.4)
        lgd = plt.legend(handles=legen, labels=names, loc='lower center', ncol=4, fontsize=14, bbox_to_anchor=(-0.60, -0.98))
        f.savefig(nomesave + "times.pdf", dpi=200, bbox_extra_artists=(lgd,), bbox_inches='tight')
        plt.close()



if __name__ == "__main__":
	main(sys.argv[1:])

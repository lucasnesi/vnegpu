#!/bin/bash

declare -a algorithms_slow=("local_metric" "generic_rank" "dijkstra" "r_kleene" "k_means")

declare -a algorithms_fast=("k_means")

run_iterations_fat_tree_cpu()
{
  for it in {1..10}
  do
    echo "Processing: $1 $it"
    ./main --algorithm=$1 --generator=fat_tree --k_value=$2 --cpu=true >> "../performance/"$1"_fat_tree_"$2"_cpu.txt"
  done
}

run_iterations_fat_tree()
{
  for it in {1..5}
  do
    echo "Processing: $1 $it"
    ./main --algorithm=$1 --generator=fat_tree --k_value=$2 >> "../performance/"$1"_fat_tree_"$2".txt"
  done
}

run_iterations_bcube_cpu()
{
  for it in {1..5}
  do
    echo "Processing: $1 $it"
    ./main --algorithm=$1 --generator=bcube --k_value=$2 --n_value=$3 --cpu=true >> "../performance/"$1"_bcube_"$2"_"$3"_cpu.txt"
  done
}

run_iterations_bcube()
{
  for it in {1..5}
  do
    echo "Processing: $1 $it"
    ./main --algorithm=$1 --generator=bcube --k_value=$2 --n_value=$3 >> "../performance/"$1"_bcube_"$2"_"$3".txt"
  done
}

run_iterations_dcell_cpu()
{
  for it in {1..5}
  do
    echo "Processing: $1 $it"
    ./main --algorithm=$1 --generator=dcell --k_value=$2 --n_value=$3 --cpu=true >> "../performance/"$1"_dcell_"$2"_"$3"_cpu.txt"
  done
}

run_iterations_dcell()
{
  for it in {1..5}
  do
    echo "Processing: $1 $it"
    ./main --algorithm=$1 --generator=dcell --k_value=$2 --n_value=$3 >> "../performance/"$1"_dcell_"$2"_"$3".txt"
  done
}

run_iterations_fit_old()
{
  for it in {1..5}
  do
    echo "Processing $it"
#    ./main --algorithm=fit --generator=fat_tree --k_value=24 --generator2=fat_tree --k_value2=12  >> "../performance/fit_fat_tree_24.txt"
    ./main --algorithm=fit --generator=fat_tree --k_value=24 --generator2=tree --k_value2=7  >> "../performance/fit_fat_tree_24.txt"
#    ./main --algorithm=fit --generator=bcube --k_value=8 --n_value=3 --generator2=fat_tree --k_value2=12  >> "../performance/fit_bcube_8_3.txt"
    ./main --algorithm=fit --generator=bcube --k_value=8 --n_value=3 --generator2=tree --k_value2=7  >> "../performance/fit_bcube_8_3.txt"
  done
}

run_iterations_fit_dcell()
{
  for it in {1..5}
  do
    echo "Processing $it"
#    ./main --algorithm=fit --generator=dcell --k_value=$1 --n_value=$2 --generator2=fat_tree --k_value2=12  >> "../performance/fit_dcell_f_"$1".txt"
    ./main --algorithm=fit --generator=dcell --k_value=$1 --n_value=$2 --generator2=tree --k_value2=7  >> "../performance/fit_dcell_"$1"_"$2".txt"
  done
}

run_iterations_fit()
{
  for it in {1..10}
  do
    echo "Processing $it"
#    ./main --algorithm=fit --generator=fat_tree --k_value=48 --generator2=fat_tree --k_value2=12 --cpu=true >> "../performance/fit_fat_tree_cpu.txt"
    #./main --algorithm=fit --generator=fat_tree --k_value=24 --generator2=tree --k_value2=7 --cpu=true >> "../performance/fit_fat_tree_24_cpu.txt"
    ./main --algorithm=fit --generator=dcell --k_value=7 --n_value=2 --generator2=tree --k_value2=7 --cpu=true >> "../performance/fit_dcell_7_2_cpu.txt"
    ./main --algorithm=fit --generator=dcell --k_value=11 --n_value=2 --generator2=tree --k_value2=7 --cpu=true >> "../performance/fit_dcell_11_2_cpu.txt"
    #./main --algorithm=fit --generator=bcube --k_value=8 --n_value=3 --generator2=tree --k_value2=7 --cpu=true >> "../performance/fit_bcube_8_3_cpu.txt"
  done
}

#for gg in {1..2}
#do
#  for alg in "${algorithms_slow[@]}"
#  do
#    run_iterations_dcell_cpu $alg 11 2
#  done
#done

#run_iterations_dcell_cpu "mcl" 7 2

run_iterations_fit

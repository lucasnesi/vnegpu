#include <stdio.h>

#include <chrono>

#include <vnegpu/graph.cuh>

#include <vnegpu/metrics.cuh>
#include <vnegpu/distance.cuh>
#include <vnegpu/allocation_policies.cuh>

#include <vnegpu/generator/fat_tree.cuh>
#include <vnegpu/generator/bcube.cuh>
#include <vnegpu/generator/dcell.cuh>

#include <vnegpu/algorithm/local_metric.cuh>
#include <vnegpu/algorithm/generic_rank.cuh>
#include <vnegpu/algorithm/dijkstra.cuh>
#include <vnegpu/algorithm/r_kleene.cuh>
#include <vnegpu/algorithm/k_means.cuh>
#include <vnegpu/algorithm/mcl.cuh>
#include <vnegpu/algorithm/fit.cuh>

#include <vnegpu/algorithm/serial/local_metric.cuh>
#include <vnegpu/algorithm/serial/generic_rank.cuh>
#include <vnegpu/algorithm/serial/dijkstra.cuh>
#include <vnegpu/algorithm/serial/r_kleene.cuh>
#include <vnegpu/algorithm/serial/k_means.cuh>
#include <vnegpu/algorithm/serial/mcl.cuh>
#include <vnegpu/algorithm/serial/fit.cuh>

#include <vnegpu/generator/request.cuh>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

enum Algorithm{
  local_metric,
  generic_rank,
  dijkstra,
  r_kleene,
  k_means,
  mcl,
  fit
};

enum Generator{
  fat_tree,
  bcube,
  dcell,
  tree_request
};

std::istream& operator>>(std::istream& in, Algorithm& cod)
{
    std::string token;
    in >> token;
    if (token == "local_metric")
        cod = local_metric;
    else if (token == "generic_rank")
        cod = generic_rank;
    else if (token == "dijkstra")
        cod = dijkstra;
    else if (token == "r_kleene")
        cod = r_kleene;
    else if (token == "mcl")
        cod = mcl;
    else if (token == "k_means")
        cod = k_means;
    else if (token == "fit")
        cod = fit;
    else
        in.setstate(std::ios_base::failbit);
    return in;
}

std::istream& operator>>(std::istream& in, Generator& cod)
{
    std::string token;
    in >> token;
    if (token == "fat_tree")
        cod = fat_tree;
    else if (token == "bcube")
        cod = bcube;
    else if (token == "dcell")
        cod = dcell;
    else if (token == "tree")
        cod = tree_request;
    else
        in.setstate(std::ios_base::failbit);
    return in;
}


int main(int ac, char* av[]){

  Algorithm selected_algorithm;
  Generator selected_generator;
  Generator selected_generator2;
  bool run_cpu;
  int k_value, n_value, k_value2, n_value2;

  try
  {
    po::options_description config("Opcoes");
    config.add_options()
        ("algorithm,A", po::value<Algorithm>(&selected_algorithm)->default_value(local_metric),"Algorithm to run.")
        ("generator,G", po::value<Generator>(&selected_generator)->default_value(fat_tree),"Generator to use.")
        ("generator2,g", po::value<Generator>(&selected_generator2)->default_value(fat_tree),"Generator 2 to use.")
        ("k_value,K", po::value<int>(&k_value)->default_value(4),"K value.")
        ("n_value,N", po::value<int>(&n_value)->default_value(2),"N value.")
        ("k_value2,k", po::value<int>(&k_value2)->default_value(4),"K value 2.")
        ("n_value2,n", po::value<int>(&n_value2)->default_value(2),"N value 2.")
        ("cpu,C", po::value<bool>(&run_cpu)->default_value(false),"Use CPU to run algorithms.");

    po::options_description cmdline_options;
    cmdline_options.add(config);

    po::variables_map vm;
    store(po::command_line_parser(ac, av).options(cmdline_options).run(), vm);
    notify(vm);

    vnegpu::graph<float>* data_center;
    vnegpu::graph<float>* request;

    if(selected_generator==fat_tree){
        data_center = vnegpu::generator::fat_tree<float>(k_value);
    }else if(selected_generator==bcube){
        data_center = vnegpu::generator::bcube<float>(k_value, n_value);
    }else if(selected_generator==dcell){
        data_center = vnegpu::generator::dcell<float>(k_value, n_value);
        //printf("DC:%d\n", data_center->get_num_nodes());
    }else if(selected_generator2==tree_request){
        int n=3;
        int v[3] = {k_value2, k_value2, k_value2};
        data_center = vnegpu::generator::request_gen<float>(n, v);
    }

    int var_index = data_center->add_node_variable("Var");

    for(int i=0; i<data_center->get_num_edges(); i++){
      data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 1);
    }

    //printf("Nos:%d\n",data_center->get_num_nodes());

    if(selected_algorithm==fit){

      if(selected_generator2==fat_tree){
          request = vnegpu::generator::fat_tree<float>(k_value2);
      }else if(selected_generator2==bcube){
          request = vnegpu::generator::bcube<float>(k_value2, n_value2);
      }else if(selected_generator2==dcell){
          request = vnegpu::generator::dcell<float>(k_value2, n_value2);
      }else if(selected_generator2==tree_request){
          int n=3;
          int v[3] = {k_value2, k_value2, k_value2};
          request = vnegpu::generator::request_gen<float>(n, v);
      }

      for(int i=0; i<data_center->get_num_nodes(); i++){
        if(data_center->get_node_type(i) == vnegpu::TYPE_HOST){
          //If the node is a host set to 10
          data_center->set_variable_node(data_center->variables.node_capacity, i, 10);
        }else{
          //else set to 1
          data_center->set_variable_node(data_center->variables.node_capacity, i, 1);
        }
      }

      //Set all edge_capacity variables of data_center graph to one.
      for(int i=0; i<data_center->get_num_edges(); i++){
        data_center->set_variable_edge_undirected(data_center->variables.edge_capacity, i, 1000);
      }

      //Set the node_capacity variable of request graph.
      for(int i=0; i<request->get_num_nodes(); i++){
        if(request->get_node_type(i) == vnegpu::TYPE_HOST)
        {
          //If the node is a host set to 1
          request->set_variable_node(request->variables.node_capacity, i, 1);
        }else{
          //else set to 0.1
          request->set_variable_node(request->variables.node_capacity, i, 0.1);
        }
      }

      //Set all edge_capacity variables of request graph to one.
      for(int i=0; i<request->get_num_edges(); i++){
        request->set_variable_edge_undirected(request->variables.edge_capacity, i, 1);
      }

      request->update_gpu();

    }

    data_center->update_gpu();

    vnegpu::util::host_matrix<float>* distance_matrix;
    if(selected_algorithm==k_means){
      if(!run_cpu){
          vnegpu::algorithm::r_kleene(data_center, data_center->variables.edge_capacity);
      }else{
          distance_matrix = vnegpu::algorithm::serial::r_kleene(data_center, data_center->variables.edge_capacity);
      }
    }


    auto start = std::chrono::steady_clock::now();
    if(!run_cpu){

      switch(selected_algorithm){
        case local_metric:
            vnegpu::algorithm::local_metric(data_center, var_index, vnegpu::metrics::degree() );
        break;
        case generic_rank:
            vnegpu::algorithm::generic_rank(data_center, var_index, vnegpu::metrics::one() );
        break;
        case dijkstra:
            vnegpu::algorithm::dijkstra(data_center, 0, var_index, vnegpu::metrics::edge_weight());
        break;
        case r_kleene:
            vnegpu::algorithm::r_kleene(data_center, data_center->variables.edge_capacity);
        break;
        case mcl:
            vnegpu::algorithm::mcl(data_center, data_center->variables.edge_capacity, 2, 1.2, 0);
        break;
        case k_means:
            vnegpu::algorithm::k_means(data_center, 6, vnegpu::distance::matrix_based());
        break;
        case fit:
            vnegpu::algorithm::fit(data_center, request, vnegpu::allocation::worst_fit());
        break;
      }

    }else{

      switch(selected_algorithm){
        case local_metric:
            vnegpu::algorithm::serial::local_metric(data_center, var_index, vnegpu::metrics::degree() );
        break;
        case generic_rank:
            vnegpu::algorithm::serial::generic_rank(data_center, var_index, vnegpu::metrics::one() );
        break;
        case dijkstra:
            vnegpu::algorithm::serial::dijkstra(data_center, 0, var_index, vnegpu::metrics::edge_weight());
        break;
        case r_kleene:
            vnegpu::algorithm::serial::r_kleene(data_center, data_center->variables.edge_capacity);
        break;
        case mcl:
            vnegpu::algorithm::serial::mcl(data_center, data_center->variables.edge_capacity, 2, 1.2, 0);
        break;
        case k_means:
            vnegpu::algorithm::serial::k_means(data_center, 6, vnegpu::distance::serial::matrix_based(), distance_matrix);
        break;
        case fit:
            vnegpu::algorithm::serial::fit(data_center, request, vnegpu::allocation::worst_fit());
        break;
      }

    }

    auto end = std::chrono::steady_clock::now();

    auto diff = end - start;

    std::cout << std::chrono::duration <double, std::milli> (diff).count() << std::endl;

    //data_center->update_cpu();

    //data_center->save_to_gexf("perform.gexf");

    data_center->free_graph();
    if(selected_algorithm==fit){
        request->free_graph();
    }
    cudaDeviceReset();
  } catch (const std::exception& e) {
    std::cout << "Error:" << e.what();
  }

  return 0;
}

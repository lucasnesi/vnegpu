#!/usr/bin/python3

import numpy as np
import sys
import csv
from pathlib import Path
import os
import glob

def precision(f):
    return "%0.3f" % f

def read_file(file):
    times = np.asarray([])
    my_file = Path(file)
    #print(file)
    if my_file.is_file():
        with open(file, 'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';')
            for row in spamreader:
                times = np.append(times, float(row[0]))
        return precision(np.mean(times)), precision(np.std(times))



def main(argv):
    path = "./*.txt"
    for file in sorted(glob.glob(path)):
        time, std = read_file(file)
        print(file + " = " + time + " std:" + std)


if __name__ == "__main__":
	main(sys.argv[1:])

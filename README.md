# VNEGPU

VNEGPU is a GPU-accelerated framework for virtual infrastructure allocation containing a set of graph algorithms in GPU.
This project is available under the MIT license.

### Folder Structure

* [build] - Scripts for compiling the program.
* [doc] - Documentation using Doxygen.
* [examples] - A set of source files demonstrating how to use the framework.
* [libs] - Aditional libraries used in the project.
* [performance] - Programs and Scripts used to measure and show the performance of the algorithms.
* [simulator] - The simulator created to validate possible allocation models.
* [tests] - Simple (and incomplete) tests to validate the algorithms.
* [vnegpu] - The source code of the framework.
    * [algorithm] - Source files for the graph algorithms. 
    * [generator] - Source files for the topologies generators.
    * [util] - General functions used in the framework.

### Compilation

The examples can be compilated running `make examples` on folder `build`.

```sh
$ cd build
$ make examples
```
Each example will create its executable named `exampleI`.


